#
# Handler of SAXO+ dictionary of simulations
# ------------------------------------------
#
# USAGE
# -----
#
#   In the scripts that launch the simulations, first define the context
#   according to the arguments:
#
#       import dictionary_handler as simu
#
#       simu.set_context(simulation = "bright1_s0_4_t9")
#       simu.set_context(controller = "refcontroller")
#
#
#   In the files where the parameter values are needed, the returned values are
#   those of the current simulation activated in the context ("bright1_s0_4_t9"
#   here) including the parameters defined for "refcontroller":
#
#       import dictionary_handler as simu
#       exp = simu.parameter("exposure_time")
#       gain1 = simu.parameter("stage1_gain")
#
#   Note that the dictionary is effectively loaded (and put in cache) the
#   first time the functions parameter() or parameters() are used.
#
#
# A set of simulations can be launched by using script "run_simu". In the
# shell, just type:
#
#       run_simu --help
#
#
# SECTIONS
# --------
#
# - IMPORTS AND PRIVATE VARIABLES
#
# - FUNCTIONS FOR CONTEXT
#       - get_context
#       - set_context
#
# - FUNCTIONS ON DICTIONARY CONTENT
#       - architectures
#       - controllers
#       - simulations
#
# - FUNCTIONS FOR PARAMETER VALUES
#       - parameter
#       - parameters
#       - change_parameter
#       - reset_parameters
#
# - UTILITY FUNCTIONS
#       - load_simulation
#       - merge_dict
#


# --+=^=+----------------------------------------------------------------------
#                      IMPORTS AND PRIVATE VARIABLES
# -----------------------------------------------------------------------------

# Import the dictionary of simulations.
import os

_my_directory, _my_name = os.path.split(os.path.abspath(__file__))
_my_cases_dir = os.path.abspath(os.path.join(_my_directory, "../cases"))
os.sys.path.insert(0, _my_cases_dir)

from dictionary_of_simulations import saxoplus_simulations as dico

import re                          # For regular expressions

# Private dictionary that stores the current context.
_current_context = dict(
  simulation = None,               # No default simulation.
  controller = 'refcontroller'    # default controller.
)

_cache = dict(
  context = tuple(),               # Context of the cached parameters.
  parameters = dict()              # Flatten dictionary of current simulation.
)


# --+=^=+----------------------------------------------------------------------
#                         FUNCTIONS FOR CONTEXT
# -----------------------------------------------------------------------------

def get_context():
    """Return the current context as a tuple.

    The returned tuple is: (simulation, controller, architecture)
    """
    return tuple(_current_context.values())


def set_context(simulation = None,
                controller = None):
    """Set the context of the simulation

    The context is defined by the names of the simulation and of the
    controller, each one as a string. Examples:

        set_context(controller = 'refcontroller')
        set_context(simulation = 'bright1_s0_4_t9')
        set_context('red1_s0_7_t9', 'refcontroller')
        set_context(simulation='bright1_s0_4_t9', controller='refcontroller')

    The list of possible values for each argument can be obtained respectively
    with functions simulations() and controllers().

    The current context can be obtained as a tuple with function get_context().
    """
    context = _current_context

    # ---- Check simulation
    #
    # - Update its value if a string within the set of known simulations.
    #
    if simulation:
        if not isinstance(simulation, str):
            raise TypeError("argument \"simulation\" is not a string")
        if simulation not in simulations():
            raise ValueError(f'simulation "{simulation}" not in dictionary')
        context['simulation'] = simulation

    # ---- Check controller
    #
    # - Update its value if a string within the set of known controllers.
    #
    if controller:
        if not isinstance(controller, str):
            raise TypeError("argument \"controller\" is not a string")
        if controller not in controllers():
            raise ValueError(f'controller "{controller}" unknown')
        context['controller'] = controller

    # ---- Reset cache of the parameters
    reset_parameters()


# --+=^=+----------------------------------------------------------------------
#                       FUNCTIONS ON DICTIONARY CONTENT
# -----------------------------------------------------------------------------


def architectures():
    """Return the list of possible architectures"""
    return ['saxo', 'standalone', 'standalone+', 'integrated']


def controllers():
    """Return a sorted list of the available controllers

    This list is obtained from the dictionary of simulations.
    """
    ctrlist = []
    for simu in list(dico):
        param = dico[simu]
        if isinstance(param, dict):
            ctrlist += list(filter(lambda x: isinstance(param[x], dict),
                            param.keys()))
    return sorted(set(ctrlist))


def simulations(pattern: str = None):
    """Return the list of simulations defined in the dictionary

    The full list is returned with no argument. A subset of the list can be
    selected with a regular expression given as the argument.
    """
    if bool(pattern):
        if not isinstance(pattern, str):
            raise TypeError("argument \"pattern\" is not a string")
        r = re.compile(pattern, re.IGNORECASE)
        simus = list(filter(r.fullmatch, list(dico)))

    else:
        simus = list(dico)

    # ---- Remove keys that begin with "_".
    # - r.match matches characters at the beginning of string.
    #
    r = re.compile('[^_]')
    return list(filter(r.match, simus))


# --+=^=+----------------------------------------------------------------------
#                        FUNCTIONS FOR PARAMETER VALUES
# -----------------------------------------------------------------------------


def parameter(name):
    """Get the value of a parameter for the current simulation.

    The simulation and its context are defined by the names of the simulation
    and of the controller, each one given as a string. They are set with the
    function `set_context()`. Use function `parameters()` to get the full list
    of the parameters.
    """
    if not isinstance(name, str):
        raise TypeError("argument \"name\" is not a string")

    param = parameters()
    if name in param:
        return param[name]
    else:
        context = get_context()
        raise NameError(f'parameter "{name}" not found in context {context}')


def parameters():
    """Return a dictionary of all the parameters of the current simulation.

    The simulation and its context are defined by the names of the simulation
    and of the controller, each one given as a string. They are set with the
    function `set_context()`. A flatten version of the parameters is saved in
    cache and reused as long as the context does not change. Use function
    `parameter()` to get the value of a parameter.
    """
    cache = _cache

    # ---- Return cache if cache is still valid
    #
    context = get_context()
    if context == cache['context']:
        return cache['parameters']

    # ---- Get simulation and controller and ensure simulation is defined
    #
    simulation, controller = context
    if not simulation:
        raise NameError("simulation name not set in context")

    # ---- Get flatten merged dictionaries for simulation and controller
    #
    # - returned dictionaries are copies.
    #
    dict_simu, dict_ctrl = load_simulation(simulation, controller)

    # ---- Check that dict_ctrl contains a known architecture
    #
    if 'architecture' not in dict_ctrl:
        raise KeyError(f'entry "architecture" is missing in controller '
                       + '"{controller}"')
    architecture = dict_ctrl['architecture']
    if architecture not in architectures():
        raise ValueError(f'architecture "{architecture}" is unknown')

    # ---- Save in cache
    #
    # - merge_dict may return an error if an entry in dict_ctrl already exists
    #   in dict_simu.
    #
    cache['parameters'] = merge_dict(dict_ctrl, dict_simu)
    cache['context'] = context

    return cache['parameters']


def change_parameter(name, value):
    """Change the value of an existing parameter of the current simulation.

    The simulation and its context are defined by the names of the simulation
    and of the controller, each one given as a string. They are set with the
    function `set_context()`. Use function `parameters()` to get the full list
    of the parameters, and function `parameter()` to get the value of a
    parameter. Use function `reset_parameters()` to reset the parameters to
    their values in the dictionary of simulations.
    """
    if not isinstance(name, str):
        raise TypeError("argument \"name\" is not a string")

    param = parameters()
    if name in param:
        param[name] = value
    else:
        context = get_context()
        raise NameError(f'parameter "{name}" not found in context {context}')


def reset_parameters():
    """Reset the parameters to their values in the dictionary of simulations.

    This reset also happens when the context is changed with `set_context()`,
    with a new simulation name or a new controller name.
    """
    cache = _cache
    cache['parameters'] = None
    cache['context'] = None


# --+=^=+----------------------------------------------------------------------
#                            UTILITY FUNCTIONS
# -----------------------------------------------------------------------------


def load_simulation(simulation, controller):
    """Return a tuple of two dictionaries (simulation, controller) merged with
    the contents of all the parent simulations.

    If the specified entry `simulation` in the general dictionary of
    simulations has an entry `same_as` pointing to a parent simulation, merge
    the entries into the ones of the parent. The process is repeated
    recursively and separately for the controller. The other controllers are
    discarted.

    Return two flatten dictionaries as a tuple (simulation, controller).
    """
    # ---- Check arguments
    #
    if not isinstance(simulation, str):
        raise TypeError("first argument is not a string")

    if not isinstance(controller, str):
        raise TypeError("second argument is not a string")

    if simulation not in dico:
        raise KeyError(f'simulation "{simulation}" not found for same_as')

    # ---- Get simulation and controller dictionaries.
    #
    dict_simu = dico[simulation].copy()
    if controller in dict_simu:
        dict_ctrl = dict_simu.pop(controller)
    else:
        dict_ctrl = dict()

    # ---- Remove other sub-dictionaries and entries starting with "_"
    #
    # - sub-dictionaries are other controllers (to be removed)
    # - r.match matches characters at the beginning of string.
    #
    r = re.compile("_")

    for key in list(dict_simu):
        if isinstance(dict_simu[key], dict) or r.match(key):
            dict_simu.pop(key)

    for key in list(dict_ctrl):
        if r.match(key):
            dict_ctrl.pop(key)

    # ---- Recursively get the merged parents of this simulation
    #
    if 'same_as' in dict_simu:
        parent_simu, parent_ctrl = load_simulation(dict_simu['same_as'],
                                                   controller)
        dict_simu.pop('same_as')
    else:
        parent_simu = {}
        parent_ctrl = {}

    # ---- Return merged with parents
    return (merge_dict(dict_simu, parent_simu.copy(), overwrite=True),
            merge_dict(dict_ctrl, parent_ctrl.copy(), overwrite=True))


def merge_dict(new_values: dict, defaults: dict={}, /, *,
               overwrite = False, noerror = False) -> dict:
    """Merge dictionary new_values into dictionary defaults.

    The (possibly new) entries of `new_values` are copied in `defaults`.
    Default is to prevent overwriting entries in `default`, unless keyword
    `overwrite` is set to True. The function issues an error if an entry to be
    copied already exists in `default`, unless keyword `noerror` is set to
    True. In this case, the function will silently prevent any overwrite.

    The returned dictionary is defaults itself. To get a modified copy of
    defaults without overwritting the original:

        merged = merge_dict(new_values, defaults.copy())
    """
    # ---- Check arguments
    #
    if not isinstance(new_values, dict):
        raise TypeError("first argument is not a dictionary")

    if not isinstance(defaults, dict):
        raise TypeError("second argument is not a dictionary")

    # ---- If defaults is empty or not given, return a copy of new_values
    #
    if not defaults:
        return new_values.copy()

    # ---- Copy new keys
    #
    else:
        if overwrite:
            for key in list(new_values):
                defaults[key] = new_values[key]
        else:
            for key in list(new_values):
                if key in defaults:
                    if noerror:
                        continue
                    else:
                        raise RuntimeError(f'entry "{key}" cannot be '
                                           + 'overwritten (write protected)')
                defaults[key] = new_values[key]
        return defaults

