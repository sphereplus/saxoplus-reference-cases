##!/usr/bin/env python
## @package   saxoPlusUtils
## @brief     Utilities to compute normalized intensity curves
## @author    C. Bechet (functions from C. Goulas LESIA)
## @date      2023/01/06

import numpy as np
import astropy.io.fits as pfits
import os
import re, shutil, tempfile
import datetime
from shesha.supervisor import stageSupervisor as StageSupervisor
from shesha.supervisor import twoStagesManager as TwoStagesManager
from saxoPlusOutputs import Outputs, OutputsType


# C-RED One max frequency for a 128x128 pixels ROI.
CREDOne_max_frequency = 7206    # [Hz]
# C-RED One excess noise factor (ENF)
CREDOne_excess_noise_factor = 1.25


def CREDOneDutyCycle(frequency):
    """Return the duty cycle of the C-RED One camera

    The camera is running in the Correlated Double Sampling mode on a ROI
    128x128 pix. With such a ROI, the maximum frame frequency is 7206 Hz. At
    this frequency the duty cycle is 50%. The duty cycle is 100% at frequency
    zero (infinite exposure time) and decreases linearly with the camera
    frequency up to the 7206 Hz maximum frequency for a 128x128 ROI.

        duty = 1 - 0.5*frequency/max_frequency,   with max_frequency = 7206 Hz

    """
    global CREDOne_max_frequency

    if frequency > CREDOne_max_frequency:
        raise ValueError("frequency must be lower than", CREDOne_max_frequency)

    return 1. - 0.5*frequency/CREDOne_max_frequency


def CREDOneThroughput(frequency):
    """Return the C-RED One Camera efficiency for a given camera frequency

    The efficiency is computed as an equivalent throughput due to camera duty
    cycle and excess noise factor.

    Duty cycle

    The camera is running in the Correlated Double Sampling mode on a ROI
    128x128 pix. With such a ROI, the maximum frame frequency is 7206 Hz. At
    this frequency the duty cycle is 50%. The duty cycle is 100% at frequency
    zero (infinite exposure time) and decreases linearly with the camera
    frequency up to the 7206 Hz maximum frequency for 128x128 ROI.

        duty = 1 - 0.5*frequency/max_frequency,   with max_frequency = 7206 Hz

    Excess noise factor

    The excess noise factor (ENF) depends on the multiplication gain. It is
    considered here for the maximum multiplication gain = ~100. From the
    documentation, the ENF is 1.25 (Finger et al 2016). For a given SNR, the
    effect of the ENF is equivalent to a throughput of 1/ENF^2.

    The function returns the product of these 2 contributions.
    """
    return CREDOneDutyCycle(frequency) / (CREDOne_excess_noise_factor**2)


def CREDOneTotalNoise(readout_noise, dark_current, sky_background, frequency):
    """Return the total additive noise of the C-RED One camera

    The returned value is the square root of the total variance of the noise
    computed by adding the variances of 3 contributions: readout noise, dark
    current, and sky background. The standard deviation of the last two must be
    multiplied by the excess noise factor, since the corresponding electrons
    are multiplied before reading. The sum is corrected in the same way as the
    incident flux for the photon noise, taking into account the excess noise
    factor of the camera. The function returns:

        total_noise = sqrt( ron^2 + F^2 * (sky + dark) * T ) / F^2

    where `ron` is the standard deviation of the readout noise, `sky` and
    `dark` are the detected electrons from sky background and the dark current
    respectively (e-/pix/s), `T` is the integration time in seconds taking the
    duty cycle of the camera into account, and `F` is the excess noise factor.
    """
    global CREDOne_excess_noise_factor

    exposure = CREDOneDutyCycle(frequency) / frequency
    excess = CREDOne_excess_noise_factor **2
    return np.sqrt(readout_noise **2 +
                   excess*(dark_current + sky_background) * exposure) / excess

def createDirForOutputs(dataDir):
    # path for your data
    timeString = datetime.datetime.today().strftime("%Y%m%d_%Hh%Mm%Ss")
    outputsDir = dataDir + timeString
    os.makedirs(os.path.join(outputsDir, 'contrastCurves'))
    os.makedirs(os.path.join(outputsDir, 'psfImages'))
    os.makedirs(os.path.join(outputsDir, 'targetPhaseStats'))
    os.makedirs(os.path.join(outputsDir, 'strehlCurves'))
    os.makedirs(os.path.join(outputsDir, 'coroImages'))
    os.makedirs(os.path.join(outputsDir, 'cmdStat'))
    os.makedirs(os.path.join(outputsDir, 'wfsStat'))
    return outputsDir

def saveOutputs(manager : TwoStagesManager, outputsDir):
    # Need to save somewhere the parameters of the simulation
    # This should return an IDENTIFIER simuId
    firstStage = manager.first_stage
    secondStage = manager.second_stage
    firstOutputsKeys = firstStage.outputs.outputsKeys
    secondOutputsKeys = secondStage.outputs.outputsKeys

    if 'strehl' in firstOutputsKeys:
        writeStrehlOutputs(firstStage, outputsDir, 'saxo')
    if 'strehl' in secondOutputsKeys:
        writeStrehlOutputs(secondStage, outputsDir, 'saxoplus')

    if 'psf' in firstOutputsKeys:
        writePsfOutputs(firstStage, outputsDir, 'saxo')
    if 'psf' in secondOutputsKeys:
        writePsfOutputs(secondStage, outputsDir, 'saxoplus')

    if 'phase' in firstOutputsKeys:
        writePhaseOutputs(firstStage, outputsDir, 'saxo')
    if 'phase' in secondOutputsKeys:
        writePhaseOutputs(secondStage, outputsDir, 'saxoplus')

    if 'corono' in firstOutputsKeys:
        writeCoroOutputs(firstStage, outputsDir, 'saxo')
    if 'corono' in secondOutputsKeys:
        writeCoroOutputs(secondStage, outputsDir, 'saxoplus')

    if 'contrast' in firstOutputsKeys:
        writeContrastOutputs(firstStage, outputsDir, 'saxo')
    if 'contrast' in secondOutputsKeys:
        writeContrastOutputs(secondStage, outputsDir, 'saxoplus')

    if 'rtc' in firstOutputsKeys:
        writeCmdTTOutputs(firstStage, outputsDir, 'saxo')
        writeRTCStatOutputs(firstStage, outputsDir, 'saxo')
    if 'rtc' in secondOutputsKeys:
        writeCmdMinMaxOutputs(secondStage, outputsDir, 'saxoplus')
        writeRTCStatOutputs(secondStage, outputsDir, 'saxoplus')

# first stage only
def writeCmdTTOutputs(stage : StageSupervisor, outputsDir, name):
    outputs = stage.outputs

    filename= os.path.join(outputsDir, 'cmdStat', 'cmdTTSigma_'+name)
    writeImageToFitsFile(outputs.cmdttSigma, outputs.strehlSampl,
                         's', filename, imgunits='arcsec')

# second stage only
def writeCmdMinMaxOutputs(stage : StageSupervisor, outputsDir, name):
    outputs = stage.outputs

    filename= os.path.join(outputsDir, 'cmdStat', 'cmdMinMax_'+name)
    writeImageToFitsFile(outputs.cmdMinMax, outputs.strehlSampl,
                         's', filename, imgunits='micron')

    filename= os.path.join(outputsDir, 'cmdStat', 'cmdNbSaturations_'+name)
    writeImageToFitsFile(outputs.cmdNbSatur, outputs.strehlSampl, 's', filename)


# both stages
def writeRTCStatOutputs(stage : StageSupervisor, outputsDir, name):
    outputs = stage.outputs

    filename= os.path.join(outputsDir, 'cmdStat', 'cmdSigma_'+name)
    writeImageToFitsFile(outputs.cmdhoSigma, outputs.strehlSampl,
                         's', filename, imgunits='micron')

    filename= os.path.join(outputsDir, 'cmdStat', 'cmdAverageMap_'+name)
    writeImageToFitsFile(outputs.cmdAvgMap, 1, '', filename, imgunits='micron')

    filename= os.path.join(outputsDir, 'cmdStat', 'cmdSigmaMap_'+name)
    writeImageToFitsFile(outputs.cmdSigmaMap, 1,
                         '', filename, imgunits='micron')

    filename= os.path.join(outputsDir, 'wfsStat', 'wfsSigma_'+name)
    writeImageToFitsFile(outputs.wfsSigma, outputs.strehlSampl, 's', filename)

    filename= os.path.join(outputsDir, 'wfsStat', 'wfsAverageMap_'+name)
    writeImageToFitsFile(outputs.wfsAvgMap, 1, '', filename)

    filename= os.path.join(outputsDir, 'wfsStat', 'wfsSigmaMap_'+name)
    writeImageToFitsFile(outputs.wfsSigmaMap, 1, '', filename)


def writeStrehlOutputs(stage : StageSupervisor, outputsDir, name):
    filename= os.path.join(outputsDir, 'strehlCurves', name)
    writeImageToFitsFile(stage.outputs.strehlVal,
                         stage.outputs.strehlSampl, 's', filename)


def writePsfOutputs(stage : StageSupervisor, outputsDir, name):
    filename = os.path.join(outputsDir, 'psfImages','psfAvg_'+name)
    writeImageToFitsFile(stage.outputs.psfAvg, stage.outputs.psfSampl,
                         'LAMBDA/D', filename)
    filename = os.path.join(outputsDir, 'psfImages', 'psfSigma_'+name)
    writeImageToFitsFile(stage.outputs.psfSigma, stage.outputs.psfSampl,
                         'LAMBDA/D', filename)
    filename = os.path.join(outputsDir, 'psfImages', 'PsfDiffraction_'+name)
    writeImageToFitsFile(stage.outputs.diffractionPattern,
                         stage.outputs.psfSampl,
                         'LAMBDA/D', filename)


def writePhaseOutputs(stage : StageSupervisor, outputsDir, name):
    filename = os.path.join(outputsDir, 'targetPhaseStats', 'phaseAvg_'+name)
    writeImageToFitsFile(stage.outputs.phaseAvg, stage.outputs.phaseSampl,
                         'meter', filename, imgunits='micron')
    filename = os.path.join(outputsDir, 'targetPhaseStats', 'phaseSigma_'+name)
    writeImageToFitsFile(stage.outputs.phaseSigma, stage.outputs.phaseSampl,
                         'meter', filename, imgunits='micron')
    strehlFromPhaseStats=np.zeros((3,stage.outputs.niter))
    strehlFromPhaseStats[0,:]=stage.outputs.phaseTip
    strehlFromPhaseStats[1,:]=stage.outputs.phaseTilt
    strehlFromPhaseStats[2,:]=stage.outputs.phaseVarNoTT
    filename = os.path.join(outputsDir,'targetPhaseStats','strehlFromPhaseStats_' \
        + name)
    writeImageToFitsFile(strehlFromPhaseStats, stage.outputs.strehlSampl,
                         's', filename, imgunits='radians sq.')


def writeCoroOutputs(stage : StageSupervisor, outputsDir, name):
    filename = os.path.join(outputsDir, 'coroImages', name)
    writeImageToFitsFile(stage.outputs.coroimg, stage.outputs.coroimgSampl,
                         'LAMBDA/D', filename, imgunits='normalized by the max')


def writeContrastOutputs(stage: StageSupervisor, outputsDir, name):
    filename = os.path.join(outputsDir,'contrastCurves', name)
    writeImageToFitsFile(np.array([stage.outputs.contrastX,
                                   stage.outputs.contrastAvg,
                                   stage.outputs.contrastSigma,
                                   stage.outputs.contrastMin,
                                   stage.outputs.contrastMax]), 1,
                         'LAMBDA/D', filename)

def writeImageToFitsFile(image, scale, units, filename, imgunits : str = None):
    """
    generalize the writing of an image output of SAXO+ simulation in associated
    fits files. input :
       image (2d array): data to be saved
       scale (float): pixel size
       units (string): units of the pixel size
       filename (string): full file name (including path)
       imgunits (string): default is none.
    example : writeImageToFitsFile(image, 0.125, 'm', 'saxopsf')
    """
    hdu=None
    hdu = pfits.PrimaryHDU()
    hdu.data = image
    hdu.header['PIXSCALE'] = scale
    hdu.header['UNITS'] = units
    hdu.header['IMGUNITS'] = imgunits
    hdu.writeto(filename+'.fits')


def sedInplace(filename, pattern, repl, newfilename : str = ''):
    '''
    Perform the pure-Python equivalent of in-place `sed` substitution: e.g.,
    `sed -i -e 's/'${pattern}'/'${repl}' "${filename}"`.
    '''
    # For efficiency, precompile the passed regular expression.
    pattern_compiled = re.compile(pattern)

    # For portability, NamedTemporaryFile() defaults to mode "w+b" (i.e., binary
    # writing with updating). This is usually a good thing. In this case,
    # however, binary writing imposes non-trivial encoding constraints trivially
    # resolved by switching to text writing. Let's do that.
    with tempfile.NamedTemporaryFile(mode='w', delete=False) as tmp_file:
        with open(filename) as src_file:
            for line in src_file:
                tmp_file.write(pattern_compiled.sub(repl, line))

    # Overwrite the original file with the munged temporary file in a
    # manner preserving file attributes (e.g., permissions).
    shutil.copystat(filename, tmp_file.name)
    if newfilename=='':
        shutil.move(tmp_file.name, filename)
    else:
        shutil.move(tmp_file.name, newfilename)




def sternBrocotRatio(x : float, x_denom : float = 1., rel_tol: float = 1e-7,
                      verbose: bool = False):
    """
    Computes an approximate ratio fraction of real X / X_DENOM by the
    Stern-Brocot algorithm, and with relative tolerance REL_TOL on X value.
    """
    if x <= 0:
        raise ValueError("X value must be strictly positive")

    if rel_tol is None:
        rel_tol = 1e-7

    if x_denom is None:
        x_denom = 1.
    if x_denom <= 0:
        raise ValueError("X_DENOM relative tolerance must be strictly positive")

    s = x/x_denom
    l = np.ones(2)
    u = np.ones(2)
    l[0] = 0
    u[1] = 0
    lu = l+u

    err = x_denom / x * lu[0] / lu[1] - 1
    while (abs(err)>=rel_tol):
        if verbose is True:
            print(lu)
        snew = lu[0]/lu[1]
        if (s>snew):
            l=lu
        elif (s<snew):
            u=lu
        else:
            return lu
        lu = l+u;
        err = lu[0]/lu[1]*x_denom/x-1


    return lu.astype(int)

def seeing2r0(seeing : float):
    """
    Compute r0(m) corresponding to SEEING value (arcseconds)
    """
    if seeing<=0:
        raise ValueError("Seeing must be strictly positive value")
    return 500e-9/seeing*180*3600/np.pi  # [m]

def set_gsmag_from_photFlux(photFlux, zerop, optthroughput):
    return -2.5*np.log10(photFlux / (zerop*optthroughput))

def saxo_corono_image_sampling(wavelength_0):
    """
    this calculation of the image sampling is done to match IRDIS pixel scale,
    according to wavelength_0
    """
    IRDIS_pixel_scale = 12.25 / 1000 / 3600  # [rad] IRDIS pixel scale
    VLT_diameter = 8                         # [m]
    return np.rad2deg(wavelength_0 * 1e-6 / VLT_diameter) / IRDIS_pixel_scale


def saxo_compute_delay(framerate, useTdmcomm: bool = True):
    """
    Compute the delay to be set in units of frame considering SAXO system characterized
    times:
    - Read-out time = 725 microseconds
    - RTC time = 80 microseconds
    - DM rise time + communication time = 35  microseconds

    The delay is set to :
      ( Tread + Trtc + Tdmcomm) x framerate [frames]
    """
    Tread = 725e-6 # SAXO camera read-out time
    Trtc = 80e-6   # SAXO RTC time
    if useTdmcomm:
        Tdmcomm = 35e-6# SAXO times for DM rise and for communication
    else:
        Tdmcomm = 0
    return (Tread+Trtc+Tdmcomm)*framerate # COMPASS delay [frames]

def saxoplus_compute_delay(framerate, useTdmcomm: bool = True):
    """
    Compute the delay to be set in units of frame considering SAXO PLUS system
    times:
    - Read-out time = 70 microseconds
    - RTC time = 100 microseconds
    - Other times (e.g. DM rise time, communication) = 300microseconds

    The delay is set to :
      ( Tread + Trtc + Tothers) x framerate [frames]
    """
    Tread = 70e-6    # SAXO+ C-red read-out time
    Trtc = 100e-6    # SAXO RTC latency (to be confirmed)
    if useTdmcomm:
        Tdmcomm = 130e-6 # DM related delays (to be refined with BMC)
    else:
        Tdmcomm = 0
    return (Tread+Trtc+Tdmcomm)*framerate # COMPASS delay [frames]

def saxo_compute_delay_for_simulated_switch(framerate, switch_delay_error: float = 0):
    """
    Compute the delay to be applied to simulate the switch between
    SPARTA and the SAXOPLUS RTC.
    """
    t1 = saxo_compute_delay(framerate, useTdmcomm=False)
    if (switch_delay_error<0):
        t1 = (1-switch_delay_error) * t1 # remove a fraction of SAXO delay

    t2=saxoplus_compute_delay(framerate, useTdmcomm=False)
    if (switch_delay_error>0):
        t2+= t1 * switch_delay_error    # add a fraction of SAXO delay
        
    t1i = int(t1)
    t2i = int(t2)
    if (t2-t2i < t1-t1i):
        return t1i-t2i+1
    else:
        return t1i-t2i


# ------------------------------------------------------------------
# Tutorial : how to compute contrast curves from a coronagraph image
# ------------------------------------------------------------------

# Use this function
from shesha.util.coronagraph_utils import compute_contrast

# Example
image = np.random.random((200, 200))    # This is your image
center = image.shape[0] // 2 - (1 / 2)  # Center of the image in pixel units
                                        # In this example the center of the image is between
                                        # the four central pixels, but it can be anywhere else
width = 2.3  # This is the width of the ring over witch contrasts are computed, in pixel units
             # Often width = lambda/D in pixel
rmin = width                # Radius of the smallest ring in pixel
rmax = image.shape[0] // 2  # Radius of the largest ring in pixel

distances, avg, std, mini, maxi = compute_contrast(image, center, rmin, rmax, width)

