import numpy as np
import os
from matplotlib import pyplot as plt
from shesha.supervisor import stageSupervisor as StageSupervisor


class Outputs:

    outputsDir = []
    stageId = 0        # int identifier of the stage (when multiple exist)
    niter = 1          # number of iteration over which outputs are gathered
    diameter = 0       # Telescope diameter (meters)
    Lambda = 0         # target 0 wavelength will be set here
    dimCmd = []        # shape of command vectors
    dimIm = []         # dimensions of images
    dimPhase = []      # dimensions of phase arrays
    dimSlopes = []     # shape of slope vectors
    pupil = []         # pupil array to mask phase
    piston = []        # piston mode over the pupil
    tip = []           # tip mode
    tilt = []          # tilt mode
    phaseSampl = 0     # sampling step for phase [meters]
    psfSampl = 0       # sampling step for PSF [lambda/D]
    psfNCenter = 0     # index of the center of the PSF
    psfNsampl = 0      # dimensions of extracted PSF
    strehlSampl = []   # iteration step [seconds] for strehl curves
    contrastX = []

    outputsKeys = []   # see OutputsType for a list of possible outputsKeys

    #
    # At every iteration...
    #
    # 1) if 'strehl' in outputsKeys
    strehlVal = []     # [SR SE, SR LE, phase var SE [rad²], phase var LE [rad²]]
    # 2) if 'rtc' in outputsKeys
    wfsphSigma = []    # Std. dev. of WFS phase in each frame.
    wfsSigma = []      # Std. dev. of WFS data in each frame.
    wfsAvgMap = []     # Mean of each slope on all the frames.
    wfsSigmaMap = []   # Std. dev. of each slope on all the frames.
    cmdhoSigma = []    # Std. dev. of HO commands (first and second stage).
    cmdttSigma = []    # Std. dev. of TT command if any.
    cmdAvgMap = []     # Mean of each actuator on all the frames.
    cmdSigmaMap = []   # Std. dev. of each actuator on all the frames.
    cmdNbSatur = []    # number of saturations in each command vector.
    cmdTotalSatur = 0  # total number of saturations over all the simulation.
    cmdMinMax = []     # min and max of each command vector.
    cmdRatioSatur = 0  # proportion of saturations.


    # 3) if 'phase' in outputsKeys
    phaseAvg = []      # Avg. phase value over the WFS
    phaseSigma = []    # Std. Dev. of phase values over the WFS
    phaseTip = []      # Tip of the phase
    phaseTilt = []     # Tilt of the phase
    phaseVarNoTT = []  # Variance of the de-tilted phase

    # At the end, along time,...
    # 4) if 'psf' in outputsKeys
    psfAvg = []        # Avg PSF alo
    psfSigma = []      # Std. Dev. of the PSF
    # 5) if 'contrast' in outputsKeys
    contrastAvg = []   # Avg Contrast over the rings
    contrastSigma = [] # Std. Dev. Contrast over the rings
    contrastMin = []   # Min of Contrast over the rings
    contrastMax = []   # Max of Contrast over the rings
    # 6) if 'corono' in outputsKeys
    coroimg = []       # Image after the coronograph
    coroimgSampl = []  # corresponding sampling
    coronotype = []    # type of coronograph

    diffractionPattern = [] # Diffraction patterm


    def __init__(self, stage : StageSupervisor, niter : int, stageId : int,
                 outputsKeys, outputsDir):
        self.outputsDir = outputsDir
        self.niter = niter
        self.stageId = stageId
        self.outputsKeys = check_outputskeys(outputsKeys)
        self.Lambda = stage.config.p_targets[0].get_Lambda()
        self.diameter = stage.config.p_tel.diam

        # reset strehls in the stage
        stage.target.reset_strehl(tar_index = 0)# reset long exp. SR on stage

        if 'strehl' in self.outputsKeys:
            # Values: [SR SE, SR LE, phase var SE [rad²], phase var LE [rad²]]
            self.strehlVal = np.zeros((4, niter))

        if 'rtc' in self.outputsKeys:
            self.dimCmd = np.shape(stage.rtc.get_command(0))
            self.cmdhoSigma = np.zeros((niter))
            self.cmdttSigma = np.zeros((niter))
            self.cmdAvgMap = np.zeros(self.dimCmd)
            self.cmdSigmaMap = np.zeros(self.dimCmd)
            self.cmdMinMax = np.zeros((2, niter))
            self.cmdNbSatur = np.zeros((niter))

            self.dimSlopes = np.shape(stage.rtc.get_slopes(0))
            self.wfsphSigma = np.zeros((niter))
            self.wfsSigma = np.zeros((niter))
            self.wfsAvgMap = np.zeros(self.dimSlopes)
            self.wfsSigmaMap = np.zeros(self.dimSlopes)

        if 'phase' in self.outputsKeys:
            self.dimPhase = np.shape(stage.target.get_tar_phase(tar_index=0))#[0] CBE 2023.11.29 fix bug
            self.pupil = stage.config.p_geom._spupil
            self.phaseAvg = np.zeros(self.dimPhase)
            self.phaseSigma = np.zeros(self.dimPhase)
            self.phaseSampl = stage.config.p_geom.get_pixsize()
            # Built the tip and tilt modes for projection
            tip=np.zeros(self.dimPhase)
            tip = tip + np.arange(self.dimPhase[0])-(self.dimPhase[0]-1)/2.
            tilt = np.transpose(np.copy(tip))
            tip *= self.pupil # pupil may not have the same shape along X and along Y
            tilt *= self.pupil
            norm_pupil = np.sqrt(np.sum(self.pupil*self.pupil))
            norm_tip = np.sqrt(np.sum(tip*tip))
            norm_tilt = np.sqrt(np.sum(tilt*tilt))
            self.piston = self.pupil/norm_pupil
            self.tip = tip/norm_tip
            self.tilt = tilt/norm_tilt
            self.phaseTip = np.zeros((niter))
            self.phaseTilt = np.zeros((niter))
            self.phaseVarNoTT = np.zeros((niter))
            self.phaseVarNoPiston = np.zeros((niter))

        if 'psf' in self.outputsKeys:
            self.dimIm = np.shape(stage.target.get_tar_image(tar_index = 0, expo_type = "se"))
            self.psfAvg = np.zeros(self.dimIm)
            self.psfSigma = np.zeros(self.dimIm)

    def update(self, stage : StageSupervisor, i : int):
        if 'strehl' in self.outputsKeys:
            # Values: [SR SE, SR LE, phase var SE [rad²], phase var LE [rad²]]
            self.strehlVal[:,i] = stage.target.get_strehl(tar_index=0)

        if 'rtc' in self.outputsKeys:
            self.wfsphSigma[i] = np.sqrt(np.var(stage.wfs.get_wfs_phase(0)))

            slopes = stage.rtc.get_slopes(0)
            self.wfsSigma[i] = np.sqrt(np.var(slopes))
            self.wfsAvgMap += slopes
            self.wfsSigmaMap += np.square(slopes)

            cmd = stage.rtc.get_command(0)
            self.cmdAvgMap += cmd
            self.cmdSigmaMap += np.square(cmd)

            if (self.stageId==1):
                self.cmdhoSigma[i] = np.sqrt(np.var(cmd[0:len(cmd)-2]))
                self.cmdttSigma[i] = np.sqrt(np.var(cmd[len(cmd)-2:len(cmd)]))
            else:
                self.cmdhoSigma[i] = np.sqrt(np.var(cmd))
                self.cmdMinMax[:,i] = [cmd.min(), cmd.max()]
                vClip = stage.vClip
                self.cmdNbSatur[i] = np.sum(cmd <= -vClip) + np.sum(cmd >= vClip)

        if 'phase' in self.outputsKeys:
            phi = stage.target.get_tar_phase(tar_index = 0)
            self.phaseAvg = self.phaseAvg + phi
            self.phaseSigma = self.phaseSigma + np.square(phi)
            phi *= 2 * np.pi/stage.config.p_targets[0].get_Lambda() # convert to radians
            self.phaseTip[i] = np.sum(phi*self.tip)
            self.phaseTilt[i] = np.sum(phi*self.tilt)
            phiNoTT = phi-self.phaseTip[i]*self.tip-self.phaseTilt[i]*self.tilt
            phiNoTT -= np.sum(phiNoTT*self.piston)*self.piston # remove piston
            # Compute variance over pupil for SE Strehl
            self.phaseVarNoTT[i] = np.sum((phiNoTT*phiNoTT)*self.pupil)/np.sum(self.pupil)
            phiNoPiston = phi- np.sum(phi*self.piston)*self.piston # remove piston
            # compute variance over pupil for LE Strehl
            self.phaseVarNoPiston[i] = np.sum((phiNoPiston*phiNoPiston)*self.pupil)/np.sum(self.pupil)

        if 'psf' in self.outputsKeys:
            self.psfAvg = self.psfAvg + stage.target.get_tar_image(tar_index = 0, expo_type="se")
            self.psfSigma = self.psfSigma + np.square(stage.target.get_tar_image(tar_index = 0, expo_type="se"))


    def finalize(self, stage : StageSupervisor):
        niter = self.niter     # number of iterations

        if ('strehl' in self.outputsKeys) or ('rtc' in self.outputsKeys):
            self.strehlSampl = stage.config.p_loop.get_ittime()

        if 'rtc' in self.outputsKeys:
            self.cmdAvgMap /= niter
            self.cmdSigmaMap = np.sqrt(self.cmdSigmaMap / niter
                                       - np.square(self.cmdAvgMap))
            self.wfsAvgMap /= niter
            self.wfsSigmaMap = np.sqrt(self.wfsSigmaMap / niter
                                       - np.square(self.wfsAvgMap))
            if (self.stageId==2):
                nact = np.prod(self.dimCmd) # number of actuators
                self.cmdTotalSatur = np.sum(self.cmdNbSatur)
                self.cmdRatioSatur = self.cmdTotalSatur/(niter * nact)

        if 'phase' in self.outputsKeys:
            self.phaseAvg = self.phaseAvg / niter
            self.phaseSigma = np.sqrt(self.phaseSigma/niter - np.square(self.phaseAvg))

        if 'psf' in self.outputsKeys:
            self.psfAvg = self.psfAvg / niter
            self.psfsigma = np.sqrt(self.psfSigma / niter - np.square(self.psfAvg))

        if 'contrast' in self.outputsKeys:
            contrastX, contrastAvg, contrastSigma, contrastMin, contrastMax = stage.corono.get_contrast(coro_index=0)
            self.contrastX = contrastX
            self.contrastAvg = contrastAvg
            self.contrastSigma = contrastSigma
            self.contrastMin = contrastMin
            self.contrastMax = contrastMax
            self.coronotype = stage.config.p_coronos[0].get_type()

        if 'corono' in self.outputsKeys:
            self.coroimg = stage.corono.get_image(coro_index=0) \
                / np.max(stage.corono.get_psf(coro_index=0))
            self.coroimgSampl = 1./stage.config.p_coronos[0].get_image_sampling()

        if 'psf' in self.outputsKeys:
            self.diffractionPattern = stage.target.get_tar_image(0, \
                                                                 expo_type='se')
            # compute sampling step in PSF images
            nPixel = stage.get_i_pupil().shape[0]# [pixel] imaging pup. support
            pixelSize = stage.config.p_geom._pixsize  # [m] pupil pixel size
            self.psfSampl = self.diameter/pixelSize/nPixel   # [pixel^{-1}]
            self.psfNCenter = self.psfAvg.shape[0]//2
            self.psfNsampl = 120


    def plot(self, outputstitle: str = ''):

        if (self.stageId==1):
            stageLeg = 'SAXO'
        else:
            stageLeg = 'SAXO+'

        SrLeg = ''
        if ('strehl' in self.outputsKeys) or ('rtc' in self.outputsKeys) or ('phase' in self.outputsKeys):
            xsr = np.arange(self.niter)*self.strehlSampl

        if 'strehl' in self.outputsKeys:
            SrLeg = 'SR {:.1f}% at {:.0f} nm'.format(self.strehlVal[1,self.niter-1]*100,self.Lambda*1e3)
            plt.figure('Strehl')
            plt.xlabel(' time (s)')
            plt.plot(xsr, self.strehlVal[0,:], label=(stageLeg+' SE'))
            plt.plot(xsr, self.strehlVal[1,:], label=(stageLeg+' LE'))
            plt.ylim((0,1))
            plt.ylabel('Strehl ratio at {:.2f} microns'.format(self.Lambda))
            plt.legend()
            plt.title(outputstitle, loc='right')
            plt.grid(visible=True, which='both')
            plt.savefig(os.path.join(self.outputsDir,'strehlCurves','Strehl.png'))

        if 'psf' in self.outputsKeys:
            plt.figure('PSF ' + stageLeg)
            psfX = np.arange(-self.psfNsampl*self.psfSampl,
                             self.psfNsampl*self.psfSampl, self.psfSampl)
            im=plt.pcolormesh(psfX, psfX,
                              np.log(self.psfAvg[self.psfNCenter-self.psfNsampl:self.psfNCenter+self.psfNsampl,
                                                 self.psfNCenter-self.psfNsampl:self.psfNCenter+self.psfNsampl]))
            plt.xlabel(r'x [$\lambda$ / D]'+'\nAvg PSF before coronograph')
            plt.ylabel(r'y [$\lambda$ / D]')
            plt.title(stageLeg + ' \n' +SrLeg, loc='left')
            plt.title(outputstitle, loc='right')
            plt.colorbar(im)
            plt.savefig(os.path.join(self.outputsDir,'psfImages',stageLeg+'_Avg.png'))
            plt.clf()
            im=plt.pcolormesh(psfX, psfX,
                              np.log(self.psfSigma[self.psfNCenter-self.psfNsampl:self.psfNCenter+self.psfNsampl,
                                                   self.psfNCenter-self.psfNsampl:self.psfNCenter+self.psfNsampl]))
            plt.xlabel(r'x [$\lambda$ / D]'+'\n Std. Dev. of PSF before coronograph')
            plt.ylabel(r'y [$\lambda$ / D]')
            plt.title(stageLeg + ' \n'+ SrLeg, loc='left')
            plt.title(outputstitle, loc='right')
            plt.colorbar(im)
            plt.savefig(os.path.join(self.outputsDir,'psfImages',stageLeg+'_Sigma.png'))


        if 'rtc' in self.outputsKeys:
            plt.figure('Control stat')
            plt.xlabel(' time (s)')
            wfsph_ = np.round(np.max(self.wfsphSigma))+1
            cmdho_ = np.round(np.max(self.cmdhoSigma))+1
            plt.plot(xsr, self.wfsSigma, label='sigma data')
            plt.plot(xsr, self.wfsphSigma / wfsph_,
                     label='sigma WFS phase / {:.0f}'.format(wfsph_))
            plt.plot(xsr, self.cmdhoSigma / cmdho_,
                     label='sigma HO cmd / {:.0f}'.format(cmdho_))

            if (self.stageId==1):
                cmdtt_ = np.round(np.max(self.cmdttSigma))+1
                plt.plot(xsr, self.cmdttSigma/cmdtt_,
                         label='sigma TT cmd / {:.0f}'.format(cmdtt_))
            plt.ylim((0,1))
            plt.legend()
            plt.title(outputstitle, loc='right')
            plt.grid(visible=True, which='both')
            plt.savefig(os.path.join(self.outputsDir,'strehlCurves','rtc_stats.png'))

        if 'phase' in self.outputsKeys:
            plt.figure('Avg Phase map '+ stageLeg )
            phX = np.arange(-self.dimPhase[0]//2*self.phaseSampl,
                            self.dimPhase[0]//2*self.phaseSampl, self.phaseSampl)
            phY = np.arange(-self.dimPhase[1]//2*self.phaseSampl,
                            self.dimPhase[1]//2*self.phaseSampl, self.phaseSampl)
            im=plt.pcolormesh(phX, phY, self.phaseAvg)
            plt.colorbar(im)
            plt.xlabel('x projected on M1 [m]\n'+r'Average residual WF [$\mu$m]')
            plt.ylabel('y projected on M1 [m]')
            plt.title(stageLeg+ ' \n'+SrLeg, loc='left')
            plt.title(outputstitle, loc='right')
            plt.savefig(os.path.join(self.outputsDir,'targetPhaseStats',stageLeg+'_Avg.png'))
            plt.close()

            plt.figure('Sigma Phase map ' + stageLeg)
            m = np.ma.masked_where(np.isnan(self.phaseSigma),self.phaseSigma)
            im=plt.pcolormesh(phX, phY, m)
            plt.colorbar(im)
            plt.xlabel('x projected on M1 [m]\n'+r'Std. Dev. residual WF [$\mu$m]')
            plt.ylabel('y projected on M1 [m]')
            plt.title(stageLeg + ' \n'+ SrLeg, loc='left')
            plt.title(outputstitle, loc='right')
            plt.savefig(os.path.join(self.outputsDir,'targetPhaseStats',stageLeg+'_Sigma.png'))

            # Add plot from phase variance and tip-tilt coeffs
            plt.figure('Strehl')
            plt.plot(xsr, np.exp(-self.phaseVarNoTT), '--', label=(stageLeg+' SE Marechal'))
            le_strehl_from_var = np.zeros((len(xsr)))
            for i in range(len(xsr)):
                # The following line does not match the COMPASS estimates of Strehl
#                le_strehl_from_var[i] = np.exp(-np.mean(self.phaseVarNoPiston[0:i+1]))

                # factor 4.94 in the equation below is the factor provided by Parenti & Sasiela
                # An additional factor 25 has been necesdsary to match the COMPASS estimates of LE Strehl
                # An explanation of these factors still needs to be provided.
                le_strehl_from_var[i] = np.exp(-np.mean(self.phaseVarNoTT[0:i+1]))/(1+4.94*25*(self.diameter/self.phaseSampl/self.Lambda*1e-6)**2*(np.var(self.phaseTip[0:i+1])+np.var(self.phaseTilt[0:i+1])))

            plt.plot(xsr, le_strehl_from_var, '--', label=(stageLeg+' LE Parenti & Sasiela'))
            plt.xlabel(' time (s)')
            plt.ylim((0,1))
            plt.ylabel('Strehl ratio at {:.2f} microns'.format(self.Lambda))
            plt.legend()
            plt.title(outputstitle, loc='right')
            plt.grid(visible=True, which='both')
            plt.savefig(os.path.join(self.outputsDir, 'targetPhaseStats',
                        'StrehlFromPhaseStats.png'))

            # Add plot of tip-tilt coeffs
            plt.figure('residual TT')
            # Compute average phase variance due to tip and tilts
            tt2nmrms = (self.Lambda*1e3/2/np.pi)/np.sqrt(np.sum(self.pupil))
            #varTip= self.phaseTip*tt2nmrms
            #varTilt= self.phaseTilt*tt2nmrms
            plt.plot(xsr, self.phaseTip*tt2nmrms, label=(stageLeg+ ' residual tip'))
            plt.plot(xsr, self.phaseTilt*tt2nmrms, label=(stageLeg+ ' residual tilt'))
            plt.xlabel(' time (s)')
            plt.ylabel('Tip and Tilt residuals (nm rms)')
            plt.legend()
            plt.title(outputstitle, loc='right')
            plt.grid(visible=True, which='both')
            plt.savefig(os.path.join(self.outputsDir, 'targetPhaseStats',
                        'TTResidualsFromPhaseStats.png'))

        if 'corono' in self.outputsKeys:
            plt.figure('Corono image '+stageLeg)
            nimg = np.shape(self.coroimg)[0]
            coroX = np.arange(-nimg//2 * self.coroimgSampl,
                              nimg//2 * self.coroimgSampl, self.coroimgSampl)
            im=plt.pcolormesh(coroX, coroX, self.coroimg)
            plt.colorbar(im)
            plt.xlabel(r'x [$\lambda$ / D]'+'\n Image after the coronograph')
            plt.ylabel(r'y [$\lambda$ / D]')
            plt.title(stageLeg+ ' \n'+SrLeg, loc='left')
            plt.title(outputstitle, loc='right')
            plt.savefig(os.path.join(self.outputsDir,'coroImages',stageLeg+'.png'))

        if 'contrast' in self.outputsKeys:
            plt.figure('contrast curves')
            plt.plot(self.contrastX, self.contrastAvg, '-o', label='intensity '+stageLeg)
            plt.plot(self.contrastX, self.contrastSigma, '--', label='sigma '+stageLeg)
            plt.xlabel(r'r [$\lambda$ / D]')
            plt.ylabel('Contrast curves after '+ self.coronotype+' coronograph')
            plt.yscale('log')
            plt.ylim(5e-7, 1e-2)
            if (self.stageId==2):
                plt.title(SrLeg, loc='left')
            plt.title(outputstitle, loc='right')
            plt.legend()
            plt.grid(visible=True, which='both')
            plt.savefig(os.path.join(self.outputsDir,'contrastCurves',stageLeg+'.png'))



def check_outputskeys(names):
    """
        Create a safe-type enum instance from bytes contents
    """
    n = len(names)
    for k in range(n):
        if not isinstance(names[k], str) or not names[k] in vars(OutputsType).values():
            raise ValueError("Invalid enumeration value for enum %s, value %s" %
                             (OutputsType, names[k]))
    return np.array(names)


class OutputsType:
    """
    Types of simulations outputs
    """
    STREHL = 'strehl'
    PSF = 'psf'
    PHASE = 'phase'
    CONTRAST = 'contrast'
    CORONO = 'corono'
    RTC = 'rtc'

