#!/usr/bin/env python
## @package   saxo_closed_loop
## @brief     script test to simulate a SAXO closed loop
## @author    C. Bechet (from template by COMPASS Team <https://github.com/ANR-COMPASS>)
"""
script test to simulate a closed loop

Usage:
  saxo_closed_loop.py <parameters_filename> [options]

with 'parameters_filename' the path to the parameters file

Options:
  -h --help          Show this help message and exit
  --brahma           Distribute data with brahma
  --bench            For a timed call
  --tao              Use yoSupervisor developed at CRAL
  -i, --interactive  keep the script interactive
  -d, --devices devices      Specify the devices
  -n, --niter niter  Number of iterations
  -g, --generic      Use generic controller
  -f, --fast         Compute PSF only during monitoring
"""
from shesha.config import ParamConfig
from docopt import docopt
from matplotlib import pyplot as plt
from tqdm import trange
import numpy as np


if __name__ == "__main__":
    arguments = docopt(__doc__)

    param_file = arguments["<parameters_filename>"]
    compute_tar_psf = not arguments["--fast"]

    config = ParamConfig(param_file)

    # Get parameters from file
    if arguments["--bench"]:
        from shesha.supervisor.benchSupervisor import BenchSupervisor as Supervisor
    elif arguments["--brahma"]:
        from shesha.supervisor.canapassSupervisor import CanapassSupervisor as Supervisor
    elif arguments["--tao"]:
        from yoSupervisor import YoSupervisor as Supervisor
    else:
        from shesha.supervisor.compassSupervisor import CompassSupervisor as Supervisor

    if arguments["--devices"]:
        config.p_loop.set_devices([
                int(device) for device in arguments["--devices"].split(",")
        ])

    if arguments["--generic"]:
        config.p_controllers[0].set_type("generic")
        print("Using GENERIC controller...")

    if arguments["--niter"]:
        config.p_loop.set_niter(int(arguments["--niter"]))

    supervisor = Supervisor(config)

    niter = supervisor.config.p_loop.niter
    strehl_val = np.zeros((4, niter))
    plt.figure(1)
    plt.ion()
    for e in trange(niter):
        supervisor.next(compute_tar_psf=compute_tar_psf)
        strehl_val[:,e]=supervisor.target.get_strehl(tar_index = 0)
        
    x = np.arange(niter)*supervisor.config.p_loop.ittime
    plt.plot(x, strehl_val[0], label='Short-exposure')
    plt.plot(x, strehl_val[1], label='Long-exposure')
    plt.xlabel(' time (s)')
    plt.ylabel('Strehl ratio at {:.2f} microns'.format(supervisor.config.p_targets[0].get_Lambda()))
    plt.ylim((0,1))
    plt.legend()
    plt.show()

    print('Final L-E SR: ', strehl_val[1,niter-1])
    
    if arguments["--interactive"]:
        from shesha.util.ipython_embed import embed
        from os.path import basename
        embed(basename(__file__), locals())
