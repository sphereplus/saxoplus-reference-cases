#
# See "Parameters and Context" section below to know how to change the
# parameters.
#
"""
Author: F.V - Modified by C. Bechet (2023.01.03)

This script is intended to be used with the saxo+ Manager ONLY!

The saxo+ manager is instantiated with the reference configuration files
saxo.py and saxo+.py

The file saxo.py contains the saxo parameter configuration file.
The file saxo+.py contains the saxo+ parameter configuration file.

COMPASS methods can afetrwards and during the script, through the saxoplus hooks be accessed using either variables:

<saxoplusmanager>, the main SAXO+ manager (which handles the synchronisation between the 2 AO stages)
<saxo>           , the First stage compass supervisor
<saxoplus>       , the Second stage compass supervisor


Usage:
  saxoPlusScript.py <controllerpath> <dataDir>

Arguments:
  <controllerpath> used to specify the path to the controller scripts for the hooks.
  <dataDir>        used to store the results of the simulations

Example:
  ipython -i saxoPlusScript.py <controllerpath> <dataDir>
"""

import numpy as np
import matplotlib.pyplot as plt
import astropy.io.fits as pfits
import os
import datetime
import dictionary_handler as simu       # for dictionary of simulations

from shesha.util.slopesCovariance import KLmodes
from saxoPlusUtils import *
from saxoPlusOutputs import Outputs, OutputsType

from tqdm import trange

from shesha.config import ParamConfig
from twoStagesManager import TwoStagesManager
from stage1Supervisor import Stage1Supervisor
from stage2Supervisor import Stage2Supervisor
from vibration_generator import VibrationGenerator
from docopt import docopt
arguments = docopt(__doc__)
controllerpath = str(arguments['<controllerpath>'])
dataDir = str(arguments['<dataDir>'])


# --------------------- Parameters and Context ------------------
#
# This file is configurated to run only one simulation, with its parameters
# taken from "dictionary_of_simulations.py".
#
# Any parameter can be changed by replacing "simu.parameter(...)" in the
# following files: this exact file, "saxo.py", "saxo+.py", and
# "refcontroller/saxoPlusCalibOnSource.py".
#
# In this file, one or several of the following parameters can be replaced by a
# list of numbers to loop across all the combinations of their values:
# "seeing", "coherenceTime", "photFluxStage1", "photFluxStage2_noCam".
#
# ---- Context
simu.set_context(simulation = "bright1_s0_4_t9")
simu.set_context(controller = "refcontroller")
# ---------------------------------------------------------------


"""
Simulation parameters
"""
# observing conditions
seeing         = [simu.parameter("seeing")]           # [arcsec]
coherenceTime  = [simu.parameter("coherenceTime")]    # [ms]

# Fluxes
# - For saxo+, flux without taking the C-RED One camera into account.
photFluxStage1 = [simu.parameter("stage1_photFlux")]  # [photons/m2/s]
photFluxStage2_noCam = [simu.parameter("stage2_photFlux")] # [photons/m2/s]

# define training time
training_time = simu.parameter("training_time")   # [s]
# define bootstrap time
bootstrap_time = simu.parameter("bootstrap_time")   # [s]
# define long exposure time
exposure_time = simu.parameter("exposure_time")     # [s]


"""
Loading of the configurations of first and second stages.
Setup of the saxoplusmanager.
USEFUL VARIABLES are: saxo, saxoplus and saxoplusmanager.
"""

config1=ParamConfig("../cases/saxo.py")
config2=ParamConfig("../cases/saxo+.py")

# Before going further, an integer frequency_ratio must exist between the two stages.
frequency_ratio = sternBrocotRatio(config1.p_loop.ittime, config2.p_loop.ittime, rel_tol=1e-2)

# need to rescale parameters in frame units according to new frequency: ittime, niter and delay.
newittime = config1.p_loop.ittime/frequency_ratio[0]
newniter = int(config1.p_loop.niter*frequency_ratio[0])
config1.p_loop.set_ittime(newittime)
config1.p_loop.set_niter(newniter)
config2.p_loop.set_ittime(newittime)
config2.p_loop.set_niter(newniter)


nControl = range(len(config1.p_controllers))
for ncontrol in nControl:
    config1.p_controllers[ncontrol].set_delay(config1.p_controllers[ncontrol].get_delay()*frequency_ratio[0])

nControl = range(len(config2.p_controllers))
for ncontrol in nControl:
    config2.p_controllers[ncontrol].set_delay(config2.p_controllers[ncontrol].get_delay()*frequency_ratio[1])

effFreqs = 1./(newittime*frequency_ratio)
print("Effectively simulated frequencies:", round(effFreqs[0]), ",", round(effFreqs[1]))

# ---- Take C-RED One camera throughtput into account for saxo+
# - photFluxStage2_noCam is the flux from the dictionary.
# - photFluxStage2 is the flux taking into account duty-cycle and excess noise
#   factor of the camera.
#
photFluxStage2 = np.array(photFluxStage2_noCam) * CREDOneThroughput(effFreqs[1])

# ---- Additive noise
# - For SAXO+, take into account sky background, readout noise, dark current,
#   duty cycle, and excess noise factor of the C-RED One camera, depending of
#   the frequency.
# - Adapt the additive noise in both stages to account for sub-exposure times.
#
noise_stage1 = config1.p_wfss[0].get_noise()
if noise_stage1 > -1:               # don't set noise if simulation is set to no noise
    noise_stage1_rescaled = np.sqrt(noise_stage1**2 / frequency_ratio[0])
    config1.p_wfss[0].set_noise(noise_stage1_rescaled)

noise_stage2 = config2.p_wfss[0].get_noise()
if noise_stage2 > -1:               # don't set noise if simulation is set to no noise
    noise_stage2 = CREDOneTotalNoise(noise_stage2, simu.parameter("stage2_dark_current"),
                              simu.parameter("stage2_sky_background"),
                              effFreqs[1])
    noise_stage2_rescaled = np.sqrt(noise_stage2**2 / frequency_ratio[1])
    config2.p_wfss[0].set_noise(noise_stage2_rescaled)
print("effective additive noise stage2 = ", noise_stage2)


first_stage = Stage1Supervisor(config1)
second_stage = Stage2Supervisor(config2)
second_stage.set_switch_delay_error(simu.parameter("switch_delay_error"))

saxoplusmanager = TwoStagesManager(first_stage, second_stage,
                                   frequency_ratio=frequency_ratio[0],
                                   frequency_ratio_denom=frequency_ratio[1])

saxo = saxoplusmanager.first_stage
saxoplus = saxoplusmanager.second_stage
saxo.manager=saxoplusmanager
saxoplus.manager=saxoplusmanager
saxoplusmanager.ittime = newittime

"""
Add some extra required configuration on supervisors: clipping on BOSTON DM.
"""
# ---- Add clipping on second stage (Boston DM)
#
# vClip=2.                    # +/- 2 microns (optical) on each actuator
# CLIPPING is made by the stage2Supervisor explicitely after set command
# We do not want COMPASS to do it, so we removed the following line.
# saxoplus.rtc._rtc.d_control[0].set_comRange(-vClip, vClip)
saxoplus.vClip = 2.         # +/- 2 microns (optical) on each actuator

"""
Add the hooks
"""
hooks_available = ['Config', 'CalibOnSource','CalibOnSky',
                   'Stage1NewSlopes', 'Stage2NewSlopes']
nhooks = len(hooks_available)
hooks=[] # initialize hooks and list of hooks
hooks_list = []
for i in range(nhooks):
    if (os.path.exists(controllerpath+'saxoPlus'+hooks_available[i]+'.py')):
        hooks_list.append(hooks_available[i])
        hooks.append(controllerpath+'saxoPlus'+hooks_available[i]+'.py')
saxoplusmanager.hooks_list = hooks_list
saxoplusmanager.hooks = hooks
saxoplusmanager.controllerpath = controllerpath



"""
Call for any further configuration required by the controller
using saxoPlusConfig.py if necessary
"""
if 'Config' in saxoplusmanager.hooks_list:
    hook_index = saxoplusmanager.hooks_list.index('Config')
    exec(open(saxoplusmanager.hooks[hook_index]).read())


"""
Prepare for On-Source Calibration.
And call saxoPlusCalibOnSource.py if necessary
"""
saxo.atmos.enable_atmos(False) # disabling turbulence
if 'CalibOnSource' in saxoplusmanager.hooks_list:
    hook_index = saxoplusmanager.hooks_list.index('CalibOnSource')
    exec(open(saxoplusmanager.hooks[hook_index]).read())


"""
Prepare for closed-loop runs
"""
saxo.atmos.enable_atmos(True) # enabling turbulence
saxo.rtc.close_loop(0) # closing loop on first stage
saxoplus.rtc.close_loop(0) # closing loop on second stage

# configure training, bootstrap and long exposure
training_iter = int(training_time / newittime)
bootstrap_iter = int(bootstrap_time / newittime)
exposure_iter = int(exposure_time / newittime)



# computing Fried parameter and compass 'gs_mag'
# according to observing conditions
r0Seeing = 500e-9/np.array(seeing)*180*3600/np.pi  # [m]

windSpeed = np.copy(saxo.config.p_atmos.windspeed)    # [m] / [s]
equivSpeed = np.sum(saxo.config.p_atmos.frac*(np.abs(windSpeed))**(5./3))**(3./5)
tau0Seeing = 0.31 * r0Seeing / equivSpeed                # [s]
for i in range(len(seeing)):
    print("r0 and tau0 with default speed:", r0Seeing[i],"m - ", tau0Seeing[i], "s")
nscreens = saxo.config.p_atmos.get_nscreens()

# creating strehl.txt file containing strehl
SimuDataBaseFile = os.path.join(dataDir,'simus.txt')
if os.path.exists(SimuDataBaseFile):
    dbfile = open(SimuDataBaseFile, 'a')
else:
    dbfile = open(SimuDataBaseFile, 'a')
    line = 'seeing [arcsec] | req. r0 [m] | eff. r0 [m] | req. tau0 [ms] | eff. tau0 [ms] |req. flux stage1 [photons/m2/s] | eff. flux [photons/subap/frame] | flux stage2 before cam [photons/m2/s] | req. flux stage2 [photons/m2/s] | eff. flux [photons/pix/frame] | avg strehl LE SAXO | sigma strehl SE SAXO | avg strehl LE SAXO+ | sigma strehl SE SAXO+ | avg contrast 4-6 l/D SAXO | avg contrast 4-6 l/D SAXO+ | prop saturation | SAXO freq | SAXO+ freq | exposure time | outputs directory |'
    dbfile.write(line + '\n')

# vibration generation

match simu.parameter("vlt_vibrations"):
    case 1:
        vib_tt_model_path = '../data/vibration_models/modelevib0606tiptilt.fits'
        vib_KL_model_path = '../data/vibration_models/modelevib0606KL.fits'
    case 2:
        vib_tt_model_path = '../data/vibration_models/modelevib0611tiptilt.fits'
        vib_KL_model_path = '../data/vibration_models/modelevib0611KL.fits'
    case 3:
        vib_tt_model_path = '../data/vibration_models/modelevib0606tiptilt_2.fits'
        vib_KL_model_path = '../data/vibration_models/modelevib0606KL_2.fits'


if simu.parameter("vlt_vibrations"):
    M2V_saxo = pfits.getdata('../data/KL_basis/M2V_saxo.fits')
    vib_tt_model_c =  pfits.open(vib_tt_model_path) # load tiptil vibration continuous model
    vib_KL_model_c =  pfits.open(vib_KL_model_path) # load KL vibration continuous model
    fs = 1/newittime
    vib_tt_gen = VibrationGenerator(vib_tt_model_c, fs)
    vib_KL_gen = VibrationGenerator(vib_KL_model_c, fs)
    vibration_voltages = np.zeros(saxo.rtc.get_command(0).shape[0]) # vibration command, size of saxo command
    saxo_pupil = saxo.config.p_geom.get_spupil()
    phase2nm = np.sqrt(np.sum(saxo_pupil))/1e3
    P_sparta2compass = phase2nm*np.array([
    [1, 0, 0, 0, 0],
    [0, 0, -1, 0, 0],
    [0, -1, 0, 0, 0],
    [0, 0, 0, -1, 0],
    [0, 0, 0, 0, 1]])

# loops
for i in range(len(seeing)):
    for j in range(len(coherenceTime)):
        speedFactor = coherenceTime[j] * 1e-3 / tau0Seeing[i] # rescaling factors for speeds
        print("Speed rescaling factor:", speedFactor)

        for k in range(len(photFluxStage1)):
            for kk in range(len(photFluxStage2)):

                prefix=createDirForOutputs(dataDir)
                # setting flux observing conditions
                saxo.set_gsmag_from_photFlux(photFluxStage1[k])
                saxoplus.set_gsmag_from_photFlux(photFluxStage2[kk])

                # setting atmospheric observing conditions
                saxo.atmos.set_r0(r0Seeing[i])
                for l in range(nscreens): # rescale wind speeds to get the correct tau0
                    saxo.atmos.set_wind(screen_index = l, windspeed=windSpeed[l]/speedFactor)

                # RESET FOR A NEW STARTING CLOSED-LOOP SIMULATION
                saxo.config.p_atmos.set_seeds([5678 + i for i in range(nscreens)])
                saxo.wfs.set_noise(0, noise_stage1_rescaled, seed = 5678) # set seed
                saxoplus.wfs.set_noise(0, noise_stage2_rescaled, seed = 5678) # set seed
                if simu.parameter("vlt_vibrations"): 
                    vib_tt_gen.set_seed(5678)
                    vib_KL_gen.set_seed(5678)
                saxoplusmanager.reset_simu()
                saxoplusmanager.reset_exposure()

                # starting training
                print("starting training")
                saxoplusmanager.simulation_state = "training"
                for e in trange(training_iter):
                    if simu.parameter("vlt_vibrations"):
                        vibration_voltages[-2:] = vib_tt_gen.step()
                        vibration_voltages[:-2] = M2V_saxo[:,2:7]@P_sparta2compass@vib_KL_gen.step()
                        saxo.rtc.set_perturbation_voltage(0,'vib',vibration_voltages)
                    saxoplusmanager.next()

                saxo.config.p_atmos.set_seeds([1234 + i for i in range(nscreens)])
                saxo.wfs.set_noise(0, noise_stage1_rescaled, seed = 1234) # set seed
                saxoplus.wfs.set_noise(0, noise_stage2_rescaled, seed = 1234) # set seed
                if simu.parameter("vlt_vibrations"): 
                    vib_tt_gen.set_seed(1234)
                    vib_KL_gen.set_seed(1234)
                saxoplusmanager.reset_simu()
                saxoplusmanager.reset_exposure()

                # starting bootstrap
                print("starting bootstrap")
                saxoplusmanager.simulation_state = "bootstrap"
                for e in trange(bootstrap_iter):
                    if simu.parameter("vlt_vibrations"):
                        vibration_voltages[-2:] = vib_tt_gen.step()
                        vibration_voltages[:-2] = M2V_saxo[:,2:7]@P_sparta2compass@vib_KL_gen.step()
                        saxo.rtc.set_perturbation_voltage(0,'vib',vibration_voltages)
                    saxoplusmanager.next()

                # starting long exposure...
                # reset metrics arrays
                outputsKeys = ['strehl','psf','rtc','phase','corono','contrast']
                saxo.outputs = Outputs(saxo, exposure_iter, 1, outputsKeys,
                                       prefix)
                saxoplus.outputs = Outputs(saxoplus, exposure_iter, 2,
                                           outputsKeys, prefix)

                print("starting long exposure")
                saxoplusmanager.simulation_state = "exposure"
                plt.ion()
                saxoplusmanager.reset_exposure()

                for e in trange(exposure_iter):
                    if simu.parameter("vlt_vibrations"):
                        vibration_voltages[-2:] = vib_tt_gen.step()
                        vibration_voltages[:-2] = M2V_saxo[:,2:7]@P_sparta2compass@vib_KL_gen.step()
                        saxo.rtc.set_perturbation_voltage(0,'vib',vibration_voltages)
                    saxoplusmanager.next() # exposure...
                    saxo.outputs.update(saxo, e)
                    saxoplus.outputs.update(saxoplus, e)

                saxoplusmanager.simulation_state = "done"
                # at the end of the exposure...
                saxo.outputs.finalize(saxo)
                saxoplus.outputs.finalize(saxoplus)

                # gather effectively simulated parameters
                r0eff = saxo.config.p_atmos.get_r0() # [m]
                tau0eff = 0.31 * r0eff / (np.sum(saxo.config.p_atmos.frac*(np.abs(saxo.config.p_atmos.get_windspeed()))**(5./3))**(3./5))    # [s]
                nphotons_sh = saxo.compute_nphotons(photFluxStage1[k],
                                                    effFreqs[0])
                nphotons_pyr = saxoplus.compute_nphotons(photFluxStage2[kk],
                                                         effFreqs[1])
                line = '{} {:.2f} {:.2f} {} {:.2f} {:.0f} {:.1f} {:.0f} {:.0f} {:.1f} {:.5f} {:.5f} {:.5f} {:.5f} {:.5f} {:.2e} {:.2e} {:.0f} {:.0f} {} {:s}'.format(seeing[i], r0Seeing[i], r0eff, coherenceTime[j], tau0eff*1e3, photFluxStage1[k], nphotons_sh, photFluxStage2_noCam[kk], photFluxStage2[kk], nphotons_pyr, np.mean(saxo.outputs.strehlVal[1,:]), np.sqrt(np.var(saxo.outputs.strehlVal[0,:])), np.mean(saxoplus.outputs.strehlVal[1,:]), np.sqrt(np.var(saxoplus.outputs.strehlVal[0,:])), saxoplus.outputs.cmdRatioSatur, np.mean(saxo.outputs.contrastAvg[3:6]), np.mean(saxoplus.outputs.contrastAvg[3:6]), effFreqs[0], effFreqs[1], exposure_time, prefix)
                dbfile.write(line + '\n')

                saveOutputs(saxoplusmanager, prefix)
                outputstitle='r0={:.2f}m tau0={:.1f}ms expo{}s freqs {:.0f} & {:.0f} Hz\n SH {:.0f}phot/sub/frame \n Pyr {:.0f}phot/pix/frame'.format(r0eff, tau0eff*1e3, exposure_time, effFreqs[0], effFreqs[1], nphotons_sh, nphotons_pyr)
                saxo.outputs.plot(outputstitle=outputstitle)
                saxoplus.outputs.plot(outputstitle=outputstitle)

                print("seeing = ", seeing[i])
                print("coherence time = ", coherenceTime[j])
                print("flux stage1 = ", photFluxStage1[k])
                print("flux stage2 = ", photFluxStage2[kk])
                print("flux stage2 before camera = ", photFluxStage2_noCam[kk])
                print("additive noise stage2 = ", config2.p_wfss[0].get_noise())
                print("SAXO  final LE SR = ",
                      saxo.outputs.strehlVal[1,exposure_iter-1])
                print("SAXO+ final LE SR = " ,
                      saxoplus.outputs.strehlVal[1,exposure_iter-1])
                print("SAXO  Avg contrast in 4-6 Lambda / D = ",
                      np.mean(saxo.outputs.contrastAvg[3:6]))
                print("SAXO+ Avg contrast in 4-6 Lambda / D = ",
                      np.mean(saxoplus.outputs.contrastAvg[3:6]))

dbfile.close()
