#
# This file is part of COMPASS <https://github.com/COSMIC-RTC/compass>
#
# COMPASS is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# COMPASS is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with COMPASS. If not, see <https://www.gnu.org/licenses/>.
#
# Copyright (C) 2011-2024 COSMIC Team

from shesha.supervisor.stageSupervisor import StageSupervisor
from shesha.supervisor.components import AtmosCompass, DmCompass, RtcCompass, TargetCompass, TelescopeCompass, WfsCompass
from shesha.supervisor.optimizers import ModalBasis, Calibration, ModalGains
import numpy as np
import time

import shesha.constants as scons

from typing import Iterable


class Stage1Supervisor(StageSupervisor):
    """ This class implements a first stage supervisor to handle compass simulation

    Attributes inherited from StageSupervisor:
        context : (CarmaContext) : a CarmaContext instance

        config : (config) : Parameters structure

        is_init : (bool) : Flag equals to True if the supervisor has already been initialized

        iter : (int) : Frame counter

        telescope : (TelescopeComponent) : a TelescopeComponent instance

        atmos : (AtmosComponent) : An AtmosComponent instance

        target : (targetComponent) : A TargetComponent instance

        wfs : (WfsComponent) : A WfsComponent instance

        dms : (DmComponent) : A DmComponent instance

        rtc : (RtcComponent) : A Rtc component instance

        basis : (ModalBasis) : a ModalBasis instance (optimizer)

        calibration : (Calibration) : a Calibration instance (optimizer)

        modalgains : (ModalGains) : a ModalGain instance (optimizer) using CLOSE algorithm
        close_modal_gains : (list of floats) : list of the previous values of the modal gains
    """

    def next(self, *, move_atmos: bool = True, nControl: int = 0,
             tar_trace: Iterable[int] = None, wfs_trace: Iterable[int] = None,
             do_control: bool = True, apply_control: bool = True,
             compute_tar_psf: bool = True, stack_wfs_image: bool = False,
             do_centroids: bool = True, compute_corono: bool=True) -> None:
        """Iterates the AO loop, with optional parameters, considering it is a first 
        stage. 

        Overload the StageSupervisor next() method to handle the SAXOplus hooks

        Kwargs:
            move_atmos: (bool): move the atmosphere for this iteration. Default is True

            nControl: (int): Controller number to use. Default is 0 (single control configuration)

            tar_trace: (List): list of targets to trace. None is equivalent to all (default)

            wfs_trace: (List): list of WFS to trace. None is equivalent to all (default)

            do_control : (bool) : Performs RTC operations if True (Default)

            apply_control: (bool): if True (default), apply control on DMs

            stack_wfs_image : (bool) : If False (default), the Wfs image is computed as       
        usual. Otherwise, a newly computed WFS image is accumulated to the previous one.     

            compute_tar_psf : (bool) : If True (default), computes the PSF at the end of the 
        iteration

            do_centroids : (bool) : If True (default), the last WFS image is stacked and centroids computation is done. WFS image must be reset before next loop (in the manager).

            compute_corono: (bool): If True (default), computes the coronagraphic image
        """
        try:
            iter(nControl)
        except TypeError:
            # nControl is not an iterable creating a list
            nControl = [nControl]

        #get the index of the first GEO controller (-1 if there is no GEO controller)
        geo_index = next(( i for i,c in enumerate(self.config.p_controllers)
            if c.type== scons.ControllerType.GEO ), -1)

        if tar_trace is None and self.target is not None:
            tar_trace = range(len(self.config.p_targets))
        if wfs_trace is None and self.wfs is not None:
            wfs_trace = range(len(self.config.p_wfss))

        if move_atmos and self.atmos is not None:
            self.atmos.move_atmos()
        # in case there is at least 1 controller GEO in the controller list : use this one only
        self.tel.update_input_phase()
        
        if ( geo_index > -1):
            nControl = geo_index

            if tar_trace is not None:
                for t in tar_trace:

                    if apply_control:
                        self.rtc.apply_control(nControl)

                    if self.atmos.is_enable:
                        self.target.raytrace(t, tel=self.tel, atm=self.atmos, ncpa=False)
                    else:
                        self.target.raytrace(t, tel=self.tel, ncpa=False)

                    if do_control and self.rtc is not None:
                        self.rtc.do_control(nControl, sources=self.target.sources)
                        self.target.raytrace(t, dms=self.dms, ncpa=True, reset=False)

        else:
            # start updating the DM shape
            if apply_control:
                for ncontrol in nControl:
                    # command buffer is updated and commands voltages update is applied
                    self.rtc.apply_control(ncontrol)
                    # Note: clipping is always made by apply_control (CBE. 2023.01.27)

            # start the propagations
            if tar_trace is not None:  # already checked at line 213?
                for t in tar_trace:
                    if self.atmos.is_enable:
                        self.target.raytrace(t, tel=self.tel, atm=self.atmos,
                                             dms=self.dms)
                    else:
                        self.target.raytrace(t, tel=self.tel, dms=self.dms)

            if wfs_trace is not None: # already checked at line 215?
                for w in wfs_trace:
                    if self.atmos.is_enable:
                        self.wfs.raytrace(w, tel=self.tel, atm=self.atmos)
                    else:
                        self.wfs.raytrace(w, tel=self.tel)

                    if not self.config.p_wfss[w].open_loop and self.dms is not None:
                        self.wfs.raytrace(w, dms=self.dms, ncpa=False, reset=False)

                    if stack_wfs_image:
                        # accumulate image during sub-integration frames
                        wfs_image = self.wfs.get_wfs_image(w)
                        self.wfs.compute_wfs_image(w)
                        self.wfs.set_wfs_image(w, self.wfs.get_wfs_image(w) + wfs_image)
                    else:
                        self.wfs.compute_wfs_image(w)
                    
            if self.rtc is not None:
                for ncontrol in nControl : # range(len(self.config.p_controllers)):                                                          
                    # modified to allow do_centroids when the WFS exposure is over. Also udeful for calibration. (CBE 2023.01.30)
                    if do_centroids:
                        self.rtc.do_centroids(ncontrol)

                    if do_control:
                        # add the call to hooks if they exist (CBE. 2023.01.04)
                        if "Stage1NewSlopes" in self.manager.hooks_list:
                            hook_index = self.manager.hooks_list.index("Stage1NewSlopes")
                            exec(open(self.manager.hooks[hook_index]).read())
                        else:
                            self.rtc.do_control(ncontrol)
        
        if compute_tar_psf:
            for tar_index in tar_trace:
                self.target.comp_tar_image(tar_index)
                self.target.comp_strehl(tar_index)

        if self.corono is not None and compute_corono:
            for coro_index in range(len(self.config.p_coronos)):
                self.corono.compute_image(coro_index)

        if self.config.p_controllers[0].close_opti and (not self.rtc._rtc.d_control[0].open_loop):
            self.modalgains.update_mgains()
            self.close_modal_gains.append(self.modalgains.get_modal_gains())

        self.iter += 1

    def reset(self):
        """ 
        Reset the simulation to return to its original state.
        Overwrites the compassSupervisor reset function, reseting explicitely the WFS image, to force a new integration of the frame.
        """
        self.atmos.reset_turbu()
        self.wfs.reset_noise()
        for w in range(len(self.config.p_wfss)):
            self.wfs.set_wfs_image(w, self.wfs.get_wfs_image(w)*0)
        for tar_index in range(len(self.config.p_targets)):
            self.target.reset_strehl(tar_index)

        self.dms.reset_dm()
        self.rtc.open_loop()
        self.rtc.close_loop()

    def reset_wfs_exposure(self):
        """                                                                              
        Reset the wfs exposure.
        """
        for w in range(len(self.config.p_wfss)):
            self.wfs.set_wfs_image(w, self.wfs.get_wfs_image(w)*0)

            
    def compute_nphotons(self, photFlux, framerate):
        """                                                                                   
        Compute the number of photons per subaperture per frame to be                         
        setup for the simulations of the Shack-Hartmann.                                      
                                                                                              
        input :                                                                               
        photFlux (float): Nb of photons per m^2 per second                                    
        framerate (float): framerate of the 40 x 40 SH wavefront sensor                       
                                                                                              
        example :                                                                             
        computeNphotons(saxo, 138e4, 1000)                                        
                                                                                            
        """
        S = (self.config.p_tel.get_diam() / self.config.p_wfss[0].get_nxsub())**2
        nphotons = photFlux / framerate * S # nphotons / frame / subaperture
        return nphotons

    def compute_flux(self):
        S = (self.config.p_tel.get_diam() / self.config.p_wfss[0].get_nxsub())**2
        photFlux = self.config.p_wfss[0].get_nphotons()/ self.config.p_loop.get_ittime() / S
        return photFlux

    def set_gsmag_from_photFlux(self, photFlux):    
        F0 = self.config.p_wfss[0].get_zerop()  # [photons/m²/s] zero point                   
        T = self.config.p_wfss[0].get_optthroughput()  # optical throughput
        gsmag = -2.5*np.log10(photFlux / (F0*T))
        self.wfs.set_gs_mag(wfs_index = 0, mag = gsmag)

    def set_delay_from_framerate(self, framerate):
        """
        Compute the delay to be set in units of frame considering SAXO system characterized 
        times:
        - Read-out time = 725 microseconds
        - RTC time = 80 microseconds
        - DM rise time + communication time = 35  microseconds

        The delay is set to :
            ( Tread + Trtc + Tdmcomm) x framerate [frames]
        """
        Tread = 725e-6 # SAXO camera read-out time
        Trtc = 80e-6   # SAXO RTC time
        Tdmcomm = 35e-6# SAXO times for DM rise and for communication
        self.rtc.set_delay(0,(Tread+Trtc+Tdmcomm)*framerate) # COMPASS delay [frames]
