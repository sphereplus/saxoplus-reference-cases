#
# SAXO+ dictionary of simulations
# -------------------------------
#
# This file gathers the modifiable parameters of all the reference simulations
# for SAXO+ project.
#
# See "dictionary_handler.py" to use this dictionary.
#
#
# Fixed versus modifiable parameters
#
#   The fixed parameters (e.g. telescope diameter) don't appear in this file
#   because they are fixed in the code. The only way to change them is to
#   change and update the code itself. The modifiable parameters that appear in
#   this file are the only parameters that can currently be changed to define
#   specific simulations.
#
#   Where needed, it is very little work to transform a fixed parameters into a
#   modifiable one that will appear in this file.
#
#
# Default modifiable parameters
#
#   In the following dictionary of simluations, the entry named "__default"
#   defines all the modifiable parameters. The full list of the parameters that
#   can be modified by the user appears here. The other entries may redefine a
#   subset of the modifiable parameters by using "same_as" to (recursively)
#   peak up the missing parameters, down to entry "__default" where we are
#   sure to find any missing parameter since all the parameters are defined in
#   "__default".
#
#   The other entries of the dictionary of simulations name and define the
#   simulations themselves.
#
#
# Controllers
#
#   A controller has a name (e.g. "refcontroller"), and its own entry as a
#   sub-dictionary of the "__default" section (see below). This subset of this
#   sub-dictionary can appear in any simulation with some modified values for
#   this specific case (gains, frequencies, etc.). Any parameter name that can
#   be understood by the controller can be defined. Any parameter name that are
#   already defined out of the controller specific sub-dictionary cannot be
#   redefined for the controller: they are "write protected" (like "seeing" for
#   instance).
#
#   A controller must define in its sub-dictionary an entry "architecture" with
#   a value "saxo", "standalone", "standalone+", or "integrated". Thus a
#   name of a controller is associated to a specific architecture.
#
#
# Context of a simulation
#
#   The context of a simulation is defined by the (unique) name of the
#   simulation and the name of the controller (default is "refcontroller").
#
#
# Rules for the parameters of a simulation.
#
#   - In the dictionary of simulations, a simulation entry can contain
#     sub-dictionaries named with controller names. An entry "architecture"
#     must appear in these sub-dictionaries (or in the parent ones).
#
#   - The collection of the parameters is done in this order of priority, by
#     recursively following the path "same_as = <parent simulation>" entries.
#
#     1/ get the parameters of the entry "controller" in entry "simulation".
#
#     2/ add the parameters from entries "controller" in the parent
#       simulations, only picking the parameters that are not already defined.
#
#     3/ repeat the same prochedure at the level of "simulation", only adding
#        the parameters that are not already defined.
#
#   Since the entry "__default" is at the root of all the simulations, and
#   since it contains all the possible parameters, we are sure that all the
#   paremeters are defined.
#
#
# Functions for using this dictionary
#
#   See dictionary_handler.py.
#
# -----------------

saxoplus_simulations = dict(     # don't change this line!

# --+=^=+----------------------------------------------------------------------
# Default entry
# -----------------------------------------------------------------------------
#
# - The default configuration is for the highest flux, and minimum fitting and
#   delay errors. It corresponds to brigh1, seeing= 0.4 arcsec, and tau0= 9ms.
#
# - This entry should define all the possible keywords.
#
__default = dict(

  # Parameters at this level should not be changed by the controller.
  #
  vlt_vibrations = 0,             # [0,1,2,3] 0 = no vibration, 1 = model 06 June 24, 
                                  # 2 = model 11 June 24, 3 = other model 06 June 24
  waffle_amp = 0.,                # [um]waffle amplitude (recommanded 0.2 to 0.4 um)
  waffle_orientation = '+',       # waffle orientation '+' or 'x'
  exposure_time = 5.,             # [s] Duration of the long exposure.
  bootstrap_time = 0.1,           # [s] Duration of the bootstrap time.
  lambda_science = 1.65,          # [microns] H band.

  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  single_layer = False,           # True => single layer, False => 35 layers

  stage1_photFlux = 1.06e7,       # [e-/m2/s] seen by camera (with QE)
  stage1_spatial_filter = "medium", # "small", "medium", or "large"

  stage2_dm_file = 'Boston28x28_flat.fits',
  stage2_lambda = 1.043,          # [microns] Y bandwidth (width = 140nm)
  stage2_modulationRadius = 3.,   # [lambda/D] This should not be changed.
  stage2_photFlux = 9373188,      # [e-/m2/s] seen by camera (with QE)
  stage2_sky_background = 14.,    # [e-/pix/s] seen by camera (with QE)
  stage2_readout_noise = 0.3,     # [e-/pix]
  stage2_dark_current = 100.,     # [e-/pix/s]
  switch_delay_error = 0.,

  paola = dict(                   # -------------- PAOLA ----------------
    training_time = 0.,           # [s] Duration of the training time.
    architecture = "standalone",
    calibrate_IM = True,          # Calibrate interaction matrix.
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.15,           # Gain of the integrator.
    stage1_nControlled = 1200,    # number of controlled modes for calib.

    stage2_frequency = 2760.,     # [Hz]
    stage2_gain = 0.2,
    stage2_nControlled = 420,     # number of controlled modes for calib.
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    training_time = 0.,           # [s] Duration of the training time.
    architecture = "standalone",
    calibrate_IM = True,          # Calibrate interaction matrix.
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.15,           # Gain of the integrator.
    stage1_nControlled = 1200,    # number of controlled modes for calib.

    stage2_frequency = 2760.,     # [Hz]
    stage2_gain = 0.2,
    stage2_nControlled = 420,     # number of controlled modes for calib.
    ___=None),

  tao_controller = dict(          # ----------- tao_controller ----------
    training_time = 0.,           # [s] Duration of the training time.
    architecture = "standalone",
    calibrate_IM = True,          # Calibrate interaction matrix.
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.3,            # Gain of the integrator.
    stage1_nControlled = 800,     # number of controlled modes for calib.
    stage2_frequency = 2760.,     # [Hz]
    stage2_gain = 0.,             # stage 2 gain needed in SAXO+.py file

    # Regularization hyperparmaters
    rhot = 0,                   # temporal regularization weight
    mut  = 1.,                  # spatial regularization weight
    mud = 1e-7,                 # projection mirrors regul. weighting
    noise = 25,     	      	# SH measurements errors stdev. [nm]
    alpha_tt = 1000,	      	# TT favor  weighting
    alpha_ho = 1,   	      	# HODM favor weighting
    integrator_gain = 0,        # if null, IMC is used
    ___=None),

  ___=None),


# --+=^=+----------------------------------------------------------------------
# bright1 (stars A-F) / exoplanets
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
# -------------- Selected case (named "Bright1 best")
bright1_s0_4_t9 = dict(
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  same_as = "__default",          # default parameters for this simulation.
  stage1_photFlux = 1.06e7,       # [e-/m2/s] seen by camera (with QE)
  stage2_photFlux = 9373188,      # [e-/m2/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 1380.,     # [Hz]
    stage2_frequency = 2760.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.15,           # Gain of the integrator.
    stage1_nControlled = 1200,    # number of controlled modes for calib.
    stage2_frequency = 2760.,     # [Hz]
    stage2_gain = 0.2,
    stage2_nControlled = 420,     # number of controlled modes for calib.
    ___=None),

  tao_controller = dict(          # ----------- refcontroller -----------
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.2,
    stage2_frequency = 2760.,     # [Hz]
    ___=None),

  ___=None),

bright1_s0_4_t5_5 = dict(
  same_as = "bright1_s0_4_t9",    # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

bright1_s0_7_t9 = dict(
  same_as = "bright1_s0_4_t9",    # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

bright1_s0_7_t5_5 = dict(
  same_as = "bright1_s0_7_t9",    # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

bright1_s0_7_t2 = dict(
  same_as = "bright1_s0_7_t9",    # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),

bright1_s1_t5_5 = dict(
  same_as = "bright1_s0_4_t5_5",  # default parameters for this simulation.
  seeing = 1.0,                   # [arcseconds]
  ___=None),

# -------------- Selected case (named "Bright1 worse")
bright1_s1_t2 = dict(
  same_as = "bright1_s1_t5_5",    # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 1380.,     # [Hz]
    stage2_frequency = 2760.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.3,            # Gain of the integrator.
    stage1_nControlled = 1200,    # number of controlled modes for calib.
    stage2_frequency = 2760.,     # [Hz]
    stage2_gain = 0.5,
    stage2_nControlled = 540,     # number of controlled modes for calib.
    ___=None),

  tao_controller = dict(          # ----------- refcontroller -----------
    stage1_frequency = 1380.,     # [Hz]
    stage1_gain = 0.3,
    stage2_frequency = 2760.,     # [Hz]
    ___=None),
  ___=None),


# --+=^=+----------------------------------------------------------------------
# bright2 (stars A-F) / exoplanets
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
bright2_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 1.53e6,       # [e-/m2/s] seen by camera (with QE)
  stage2_photFlux = 1485550,      # [e-/m2/s] seen by camera (with QE)
  ___=None),

bright2_s0_4_t5_5 = dict(
  same_as = "bright2_s0_4_t9",    # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

bright2_s0_7_t9 = dict(
  same_as = "bright2_s0_4_t9",    # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

bright2_s0_7_t5_5 = dict(
  same_as = "bright2_s0_7_t9",    # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

bright2_s0_7_t2 = dict(
  same_as = "bright2_s0_7_t9",    # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),

bright2_s1_t5_5 = dict(
  same_as = "bright2_s0_4_t5_5",  # default parameters for this simulation.
  seeing = 1.0,                   # [arcseconds]
  ___=None),

bright2_s1_t2 = dict(
  same_as = "bright2_s1_t5_5",    # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


# --+=^=+----------------------------------------------------------------------
# bright3 (stars G-K) / exoplanets
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
bright3_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 2.43e5,       # [e-/m2/s] seen by camera (with QE)
  stage2_photFlux = 772489,       # [e-/m2/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 600.,      # [Hz]
    stage2_frequency = 2400.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 600.,      # [Hz]
    stage2_frequency = 2400.,     # [Hz]
    ___=None),
  ___=None),

bright3_s0_4_t5_5 = dict(
  same_as = "bright3_s0_4_t9",    # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

bright3_s0_7_t9 = dict(
  same_as = "bright3_s0_4_t9",    # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

bright3_s0_7_t5_5 = dict(
  same_as = "bright3_s0_7_t9",    # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

bright3_s0_7_t2 = dict(
  same_as = "bright3_s0_7_t9",    # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),

bright3_s1_t5_5 = dict(
  same_as = "bright3_s0_4_t5_5",  # default parameters for this simulation.
  seeing = 1.0,                   # [arcseconds]
  ___=None),

bright3_s1_t2 = dict(
  same_as = "bright3_s1_t5_5",    # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


# --+=^=+----------------------------------------------------------------------
# red1 (star early M) / disks
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
red1_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 2.92e4,       # [e-/m2/s] seen by camera (with QE)
  stage2_lambda = 1.144,          # [microns] YJ bandwidth (width = 380nm)
  stage2_photFlux = 733000,       # [e-/m2/s] seen by camera (with QE)
  stage2_sky_background = 41.,    # [e-/pix/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 300.,      # [Hz]
    stage2_frequency = 2760.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 300.,      # [Hz]
    stage1_nControlled = 800,     # number of controlled modes for calib.
    stage2_frequency = 2760.,     # [Hz]
    stage2_nControlled = 400,     # number of controlled modes for calib.
    ___=None),
  ___=None),

red1_s0_4_t5_5 = dict(
  same_as = "red1_s0_4_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red1_s0_7_t9 = dict(
  same_as = "red1_s0_4_t9",       # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

red1_s0_7_t5_5 = dict(
  same_as = "red1_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

# -------------- Selected case (named "Red1 fast")
red1_s0_7_t2 = dict(
  same_as = "red1_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency =  552.,     # [Hz]
    stage2_frequency = 2760.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency =  552.,     # [Hz]
    stage1_gain = 0.05,           # Gain of the integrator.
    stage1_nControlled = 400,     # number of controlled modes for calib.
    stage2_frequency = 2760.,     # [Hz]
    stage2_gain = 0.4,
    stage2_nControlled = 540,     # number of controlled modes for calib.
    ___=None),

  tao_controller = dict(          # ----------- refcontroller -----------
    stage1_frequency = 552.,      # [Hz]
    stage1_gain = 0.05,
    stage1_nControlled = 400,
    stage2_frequency = 2760.,     # [Hz]
    ___=None),

  ___=None),

red1_s1_t5_5 = dict(
  same_as = "red1_s0_4_t5_5",     # default parameters for this simulation.
  seeing = 1.0,                   # [arcseconds]
  ___=None),

red1_s1_t2 = dict(
  same_as = "red1_s1_t5_5",       # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


# --+=^=+----------------------------------------------------------------------
# red2 (star K-type) / disks
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
red2_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 1.27e4,       # [e-/m2/s] seen by camera (with QE)
  stage2_lambda = 1.144,          # [microns] YJ bandwidth (width = 380nm)
  stage2_photFlux = 184000,       # [e-/m2/s] seen by camera (with QE)
  stage2_sky_background = 41.,    # [e-/pix/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 150.,      # [Hz]
    stage2_frequency = 1250.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 150.,      # [Hz]
    stage1_nControlled = 800,     # number of controlled modes for calib.
    stage2_frequency = 1250.,     # [Hz]
    stage2_nControlled = 400,     # number of controlled modes for calib.
    ___=None),
  ___=None),

red2_s0_4_t5_5 = dict(
  same_as = "red2_s0_4_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red2_s0_7_t9 = dict(
  same_as = "red2_s0_4_t9",       # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

red2_s0_7_t5_5 = dict(
  same_as = "red2_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red2_s0_7_t2 = dict(
  same_as = "red2_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


# --+=^=+----------------------------------------------------------------------
# red3 (star early M) / disks
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
red3_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 2.66e3,       # [e-/m2/s] seen by camera (with QE)
  stage2_lambda = 1.144,          # [microns] YJ bandwidth (width = 380nm)
  stage2_photFlux = 168000,       # [e-/m2/s] seen by camera (with QE)
  stage2_sky_background = 41.,    # [e-/pix/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 50.,       # [Hz]
    stage2_frequency = 1250.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 50.,       # [Hz]
    stage1_nControlled = 800,     # number of controlled modes for calib.
    stage2_frequency = 1250.,     # [Hz]
    stage2_nControlled = 400,     # number of controlled modes for calib.
    ___=None),
  ___=None),

red3_s0_4_t5_5 = dict(
  same_as = "red3_s0_4_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red3_s0_7_t9 = dict(
  same_as = "red3_s0_4_t9",       # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

# -------------- Selected case (named "Red3 medium")
red3_s0_7_t5_5 = dict(
  same_as = "red3_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 50.,       # [Hz]
    stage2_frequency = 1250.,     # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 50.,       # [Hz]
    stage1_gain = 0.05,           # Gain of the integrator.
    stage1_nControlled = 400,     # number of controlled modes for calib.
    stage2_frequency = 1250.,     # [Hz]
    stage2_gain = 0.3,
    stage2_nControlled = 510,     # number of controlled modes for calib.
    ___=None),
  tao_controller = dict(          # ----------- refcontroller ----------
    stage1_frequency = 50.,       # [Hz]
    stage1_gain = 0.05,           # Gain of the integrator.
    stage1_nControlled = 400,     # number of controlled modes for calib.
    stage2_frequency = 1250.,     # [Hz]
    ___=None),
  ___=None),

red3_s0_7_t2 = dict(
  same_as = "red3_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


# --+=^=+----------------------------------------------------------------------
# red4 (star late M) / disks
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
red4_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 1.27e3,       # [e-/m2/s] seen by camera (with QE)
  stage2_lambda = 1.144,          # [microns] YJ bandwidth (width = 380nm)
  stage2_photFlux = 43300,        # [e-/m2/s] seen by camera (with QE)
  stage2_sky_background = 41.,    # [e-/pix/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 30.,       # [Hz]
    stage2_frequency = 600.,      # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 30.,       # [Hz]
    stage1_nControlled = 400,     # number of controlled modes for calib.
    stage2_frequency = 600.,      # [Hz]
    stage2_nControlled = 400,     # number of controlled modes for calib.
    ___=None),
  ___=None),

red4_s0_4_t5_5 = dict(
  same_as = "red4_s0_4_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red4_s0_7_t9 = dict(
  same_as = "red4_s0_4_t9",       # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

red4_s0_7_t5_5 = dict(
  same_as = "red4_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red4_s0_7_t2 = dict(
  same_as = "red4_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


# --+=^=+----------------------------------------------------------------------
# red5 (star late M) / disks
# -----------------------------------------------------------------------------
#
# Source: https://atrium.in2p3.fr/8893d699-de4e-427e-8a40-fa4bbc632a33
#
# -------------- Selected case (named "Red5 best")
red5_s0_4_t9 = dict(
  same_as = "__default",          # default parameters for this simulation.
  exposure_time = 10.,            # [s] Duration of the long exposure.
  seeing = 0.4,                   # [arcseconds]
  coherenceTime = 9.,             # [ms]
  stage1_photFlux = 3.2e2,        # [e-/m2/s] seen by camera (with QE)
  stage2_lambda = 1.144,          # [microns] YJ bandwidth (width = 380nm)
  stage2_photFlux = 15700,        # [e-/m2/s] seen by camera (with QE)
  stage2_sky_background = 41.,    # [e-/pix/s] seen by camera (with QE)

  paola = dict(                   # -------------- PAOLA ----------------
    stage1_frequency = 10.,       # [Hz]
    stage2_frequency = 300.,      # [Hz]
    ___=None),

  refcontroller = dict(           # ----------- refcontroller -----------
    stage1_frequency = 10.,       # [Hz]
    stage1_gain = 0.05,           # Gain of the integrator.
    stage1_nControlled = 390,     # number of controlled modes for calib.
    stage2_frequency = 300.,      # [Hz]
    stage2_gain = 0.3,
    stage2_nControlled = 380,     # number of controlled modes for calib.
    ___=None),
  tao_controller = dict(          # ----------- refcontroller -----------
    stage1_frequency = 10.,       # [Hz]
    stage1_gain = 0.05,           # Gain of the integrator.
    stage1_nControlled = 390,     # number of controlled modes for calib.
    stage2_frequency = 300.,      # [Hz]
    ___=None),
  ___=None),

red5_s0_4_t5_5 = dict(
  same_as = "red5_s0_4_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red5_s0_7_t9 = dict(
  same_as = "red5_s0_4_t9",       # default parameters for this simulation.
  seeing = 0.7,                   # [arcseconds]
  ___=None),

red5_s0_7_t5_5 = dict(
  same_as = "red5_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 5.5,            # [ms]
  ___=None),

red5_s0_7_t2 = dict(
  same_as = "red5_s0_7_t9",       # default parameters for this simulation.
  coherenceTime = 2.,             # [ms]
  ___=None),


___=None)   # End of the dictionary. Don't change this line!


