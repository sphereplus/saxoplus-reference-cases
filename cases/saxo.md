# Details on file saxo.md

The file [`saxo_alone.py`](./saxo.py) is a configuration file for simulating SAXO.
The intention is to be as close as possible to the real SAXO hardware. 

[[_TOC_]]

## Fixed settings

This section describes the parameters that are considered as fixed and why.
These parameters are denoted with symble `/!\` in the file
[`saxo.py`](./saxo.py).

### AO Geometry

- Pupil size

- `p_wfs0.set_fracsub(0.5)` selects the `1240` valid subapertures of SAXO.

- with the custom DM specified SAXO_HO_DM.fits, `p_dm0.set_thresh(-0.5)` allows to select the 1377 actuators of SAXO.

- command vector has 1379 components = [1377] first for the HO DM and [2] next for the TT mirror.


### Delay

These details come from CGo (Oct. 2022).

The delay was originally measured at 2.14 frames at 1200 Hz ([Beuzit et al
2019, page 10](https://arxiv.org/pdf/1902.04080.pdf)). Then the loop was
upgraded to 1380 Hz but the delay had not been remeasured at that time. I
was measured on the sky a few years ago at 1.56 ms, or about 2.15 frames at
1380 Hz ([Cantalloube et al 2020, page
3](https://arxiv.org/pdf/2003.05794.pdf)). Thus we can set the delay to
`set_delay(1.15)` at this point.

**Caveat**: `set_delay` parameter does not count the integration frame.
Thus setting `set_delay(1.15)` means a total delay `d = 2.15`.

As specified by the [COMPASS
manual](https://compass.pages.obspm.fr/website/page/manual/) in section
*voltage computation*, the fractional delay `d=2.15` frames is applied this
way. The voltage vector `v[k]` applied at iteration `k` on the DM is
computed as:

```
v[k] = a * c[k-e] + b * c[k-e-1] + p[k]
```

with: `e = floor(d) = 2`, `b = d-floor(d) = 0.15`, and `a = 1-b = 0.85`.
`p[k]` is a possible perturbation voltage set by the user.

Thus in our case:

```
v[k] = 0.85 * c[k-2] + 0.15 * c[k-3] + p[k]
```

**Caveat**: previous equation shows that the delay is not applied to the
perturbation `p[k]`. Using `p[k]` for applying an external controler
supposes that some code is added to store previous commands and to apply the
delay set in the settings. In order to apply the same delay to all external 
controlers, the controler must send its newly compute command using the 
set_command() function.



**Sign conventions**: Since `unitpervolt=1` in the parameters file, note that 
to correct an incoming wavefront `a` the voltage vector must be set 
to `v=-a` in COMPASS.

## Settings driven by the supervisor

To be continued.
