import numpy as np
from scipy.linalg import solve_discrete_lyapunov, solve_lyapunov, eig, expm, inv, sqrtm
import astropy.io.fits as pfits
from scipy.fft import fft
import matplotlib.pyplot as plt
import control as ct
import sys
sys.path.append('../../scripts')
from vibration_generator import VibrationGenerator

def ajuste_sigmav(Aphi, Gammaphi, Cphi, datavar, option=1):
    """
    Adjust the covariance matrix of the noise driving the identified discrete model.

    Parameters:
        Aphi, Gammaphi, Cphi: State model of the form:
                              xphi(k+1) = Aphi * xphi(k) + Gammaphi * v(k)
                              phi(k) = Cphi * xphi(k)
                              or
                              d(xphi(t)) = Aphi * xphi(t) dt + Gammaphi * dv(t)
                              phi(t) = Cphi * xphi(t)
        datavar: Variance/covariance matrix of phi or trajectory used for identification
        option: 1 for discrete model (default), 2 for continuous model

    Returns:
        Sigmav: Variance of the noise driving the model
    """

    # Define nphi as the number of rows in Cphi
    nphi = Cphi.shape[0]

    # Calculate Sigmaphi
    if datavar.shape[1] == nphi:
        Sigmaphi = datavar
    else:
        ntraj = datavar.shape[1]
        datavar = datavar - np.mean(datavar, axis=1, keepdims=True)
        Sigmaphi = np.zeros((nphi, nphi))
        for j in range(ntraj):
            Sigmaphi += np.outer(datavar[:, j], datavar[:, j])
        Sigmaphi /= ntraj

    # Adjustment using Lyapunov
    Sigmavtestmat = []
    MM = []
    toto = np.zeros((nphi, nphi))

    for i in range(nphi):
        for j in range(i + 1):
            Sigmavtestk = toto.copy()
            Sigmavtestk[i, i] = 1
            if j < i:
                Sigmavtestk[j, j] = 1
                Sigmavtestk[i, j] = 1
                Sigmavtestk[j, i] = 1
            Sigmavtestmat.append(Sigmavtestk.flatten())
            if option == 1:
                Sigmaxtestk = solve_discrete_lyapunov(Aphi, Gammaphi @ Sigmavtestk @ Gammaphi.T)
            else:
                Sigmaxtestk = solve_lyapunov(Aphi, Gammaphi @ Sigmavtestk @ Gammaphi.T)
            varphitestk = Cphi @ Sigmaxtestk @ Cphi.T
            MM.append(varphitestk.flatten())

    Sigmavtestmat = np.array(Sigmavtestmat).T
    MM = np.array(MM).T
    coefsSigmav = np.linalg.lstsq(MM, Sigmaphi.flatten(), rcond=None)[0]
    SigmavL = Sigmavtestmat @ coefsSigmav
    Sigmav = SigmavL.reshape((nphi, nphi))

    # Check positivity
    minvp = np.min(np.abs(eig(Sigmav)[0]))
    if minvp < 0:
        print("** Warning: Sigmav has negative eigenvalues **")

    return Sigmav


def rediscret(Ac, Gammac, Cc, varphi, Fe):
    """
    Rediscretize continuous-time shaping filter.

    Parameters:
        Ac, Gammac, Cc: Continuous-time state-space matrices.
        varphi: Variance of output.
        Fe: Sampling frequency (Hertz).

    Returns:
        Ad, Gammad, Cd: Discrete-time state-space matrices.
        RacSigmaX: Square root of steady-state variance matrix for filter initialization.
    """

    # Sampling time
    Te = 1 / Fe

    # Discretization of state matrices
    Ad = expm(Ac * Te)
    XX = inv(Ac) @ (Ad - np.eye(Ad.shape[0]))
    Cd = Cc @ XX / Te
    Gammad0 = XX @ Gammac

    # Adjust discrete filter gain
    Sigmav = ajuste_sigmav(Ad, Gammad0, Cd, varphi)
    Gammad = Gammad0 @ sqrtm(Sigmav)

    # Square root of steady-state variance matrix (for initialization)
    SigmaX = solve_discrete_lyapunov(Ad, Gammad @ Gammad.T)
    RacSigmaX = sqrtm(SigmaX)
    RacSigmaX = np.real(RacSigmaX)

    return Ad, Gammad, Cd, RacSigmaX

if __name__ == "__main__":
    model_number = 1

    match model_number:
        case 0:
            model_path = '../../data/vibration_models/modelevib0606tiptilt.fits'
        case 1:
            model_path = '../../data/vibration_models/modelevib0611tiptilt.fits'
        case 2:
            model_path = '../../data/vibration_models/modelevib0606tiptilt_2.fits'

    model_c =  pfits.open(model_path)
    data_hdu = model_c[0]
    Avibc = model_c[1].data
    Cvibc = model_c[2].data
    Gammavibc = model_c[3].data
    varvibration = model_c[4].data

    
    fs = 3000
    Te2 = 1 / fs
    Avib2, Gammavib2, Cvib2, RacSigmaX = rediscret(Avibc, Gammavibc, Cvibc, varvibration, fs)
    vib_gen = VibrationGenerator(model_c, fs)

    # Simulation parameters
    tsimul = 50
    nsimul = round(tsimul / Te2) - 1
    print(
        "*******************************************************************************************************************")
    print(f"**  tip & tilt model {model_number} -- Sampling frequency = {fs} Hz -- simulation time = {tsimul} sec. **")
    print("                                   **** simulation under way ****")

    # Initialize model state
    nxvib = Avib2.shape[0]
    xvibk = RacSigmaX @ np.random.randn(nxvib, 1)
    phivib = Cvib2 @ xvibk

    # Simulation loop
    for k in range(nsimul):
        # vk = np.random.randn(2, 1)
        # xvibk = Avib2 @ xvibk + Gammavib2 @ vk
        # phivibk = Cvib2 @ xvibk
        phivibk = vib_gen.step()
        phivib = np.hstack((phivib, phivibk[:,np.newaxis]))


    varmodvib = Cvib2 @ solve_discrete_lyapunov(Avib2, Gammavib2 @ Gammavib2.T) @ Cvib2.T
    varphivib = np.cov(phivib)
    print("\n  theoretical variance :")
    print(varmodvib)
    print("  variance of trajectory :")
    print(varphivib)
    print(
        "*******************************************************************************************************************")

    # Plot trajectories and compare Power Spectral Densities (PSDs)
    ouiplot = True
    if ouiplot:
        # Plot trajectory
        tsim = np.arange(0, tsimul, Te2)
        for tiptilt in range(2):
            plt.figure()
            plt.plot(tsim, phivib[tiptilt, :], 'm')
            plt.xlabel('time (s)')
            plt.ylabel('amplitude (arcsec)')
            plt.title('Simulated tip trajectory' if tiptilt == 0 else 'Simulated tilt trajectory')
            plt.grid()

        # PSD of trajectory
        nptraj = phivib.shape[1]
        ftraj = np.arange(0, nptraj // 2) * fs / nptraj
        npdsp = len(ftraj)

        # Model PSD
        w = ftraj * 2 * np.pi

        # Initialize a matrix to hold the frequency response data for each input-output pair
        num_outputs = Cvib2.shape[0]
        num_inputs = Gammavib2.shape[1]


        Hvib2 = ct.ss(Avib2, Gammavib2, Cvib2, 0, dt=Te2)
        gainHvib2, phase, omega = ct.frequency_response(Hvib2, ftraj * 2 * np.pi)
        dspmodvib2 = np.zeros((2, len(ftraj)))

        fig, axes = plt.subplots(2, 2, figsize=(12, 10))  # 2 rows (tip and tilt), 2 columns (PSD and cumulative PSD)

        for tiptilt in range(2):
            # Compute dspmodvib2 as in MATLAB, summing the squared gain contributions from both inputs
            dspmodvib2 = np.zeros_like(ftraj)
            dspmodvib2 = (
                np.squeeze(gainHvib2[tiptilt, 0, :]) ** 2 + np.squeeze(gainHvib2[tiptilt, 1, :]) ** 2
            ) / fs

            # Calculate the PSD for the simulated data using FFT
            XX = (np.abs(np.fft.fft(phivib[tiptilt, :])) ** 2) / nptraj / fs
            dsptraj = XX[:npdsp]  # Take only positive frequency components
            varvib2 = varphivib[tiptilt, tiptilt]
            varvibm2 = varmodvib[tiptilt, tiptilt]

            # PSD Plot
            ax1 = axes[tiptilt, 0]
            ax1.loglog(ftraj, dsptraj, 'm', label='Simulated vibration', linewidth=1.5)
            ax1.loglog(ftraj, dspmodvib2, 'k', label='Vibration model', linewidth=1.5)
            ax1.legend()
            ax1.set_ylabel('PSD (arcsec^2/Hz)')
            ax1.set_title('Tip Vibration' if tiptilt == 0 else 'Tilt Vibration')
            ax1.grid(True)

            # Cumulative PSD Plot
            cumulative_dsptraj = np.cumsum(dsptraj) * varvib2 / np.sum(dsptraj)
            cumulative_dspmodvib2 = np.cumsum(dspmodvib2) * varvibm2 / np.sum(dspmodvib2)

            ax2 = axes[tiptilt, 1]
            ax2.semilogx(ftraj, cumulative_dsptraj, 'm', label='Simulated vibration', linewidth=1.5)
            ax2.semilogx(ftraj, cumulative_dspmodvib2, 'k', label='Vibration model', linewidth=1.5)
            ax2.set_xlabel('Frequency (Hz)')
            ax2.set_ylabel('Cumulative PSD (arcsec^2)')
            ax2.legend()
            ax2.grid(True)

        # Add a main title for the entire figure
        plt.suptitle("TipTilt model and simulated trajectory PSD and cumulative PSD for model {}".format(model_number), fontsize=16)

        # Adjust layout to accommodate the main title
        plt.tight_layout(rect=[0, 0, 1, 0.95])  # Leave space for the main title
    plt.show()
