import numpy as np
from scipy.linalg import solve_discrete_lyapunov, solve_lyapunov, eig, expm, inv, sqrtm
import astropy.io.fits as pfits
from scipy.fft import fft
import matplotlib.pyplot as plt
import control as ct
# from vibration_generator import vibration_generator

def ajuste_sigmav(Aphi, Gammaphi, Cphi, datavar, option=1):
    """
    Adjust the covariance matrix of the noise driving the identified discrete model.

    Parameters:
        Aphi, Gammaphi, Cphi: State model of the form:
                              xphi(k+1) = Aphi * xphi(k) + Gammaphi * v(k)
                              phi(k) = Cphi * xphi(k)
                              or
                              d(xphi(t)) = Aphi * xphi(t) dt + Gammaphi * dv(t)
                              phi(t) = Cphi * xphi(t)
        datavar: Variance/covariance matrix of phi or trajectory used for identification
        option: 1 for discrete model (default), 2 for continuous model

    Returns:
        Sigmav: Variance of the noise driving the model
    """

    # Define nphi as the number of rows in Cphi
    nphi = Cphi.shape[0]

    # Calculate Sigmaphi
    if datavar.shape[1] == nphi:
        Sigmaphi = datavar
    else:
        ntraj = datavar.shape[1]
        datavar = datavar - np.mean(datavar, axis=1, keepdims=True)
        Sigmaphi = np.zeros((nphi, nphi))
        for j in range(ntraj):
            Sigmaphi += np.outer(datavar[:, j], datavar[:, j])
        Sigmaphi /= ntraj

    # Adjustment using Lyapunov
    Sigmavtestmat = []
    MM = []
    toto = np.zeros((nphi, nphi))

    for i in range(nphi):
        for j in range(i + 1):
            Sigmavtestk = toto.copy()
            Sigmavtestk[i, i] = 1
            if j < i:
                Sigmavtestk[j, j] = 1
                Sigmavtestk[i, j] = 1
                Sigmavtestk[j, i] = 1
            Sigmavtestmat.append(Sigmavtestk.flatten())
            if option == 1:
                Sigmaxtestk = solve_discrete_lyapunov(Aphi, Gammaphi @ Sigmavtestk @ Gammaphi.T)
            else:
                Sigmaxtestk = solve_lyapunov(Aphi, Gammaphi @ Sigmavtestk @ Gammaphi.T)
            varphitestk = Cphi @ Sigmaxtestk @ Cphi.T
            MM.append(varphitestk.flatten())

    Sigmavtestmat = np.array(Sigmavtestmat).T
    MM = np.array(MM).T
    coefsSigmav = np.linalg.lstsq(MM, Sigmaphi.flatten(), rcond=None)[0]
    SigmavL = Sigmavtestmat @ coefsSigmav
    Sigmav = SigmavL.reshape((nphi, nphi))

    # Check positivity
    minvp = np.min(np.abs(eig(Sigmav)[0]))
    if minvp < 0:
        print("** Warning: Sigmav has negative eigenvalues **")

    return Sigmav


def rediscret(Ac, Gammac, Cc, varphi, Fe):
    """
    Rediscretize continuous-time shaping filter.

    Parameters:
        Ac, Gammac, Cc: Continuous-time state-space matrices.
        varphi: Variance of output.
        Fe: Sampling frequency (Hertz).

    Returns:
        Ad, Gammad, Cd: Discrete-time state-space matrices.
        RacSigmaX: Square root of steady-state variance matrix for filter initialization.
    """

    # Sampling time
    Te = 1 / Fe

    # Discretization of state matrices
    Ad = expm(Ac * Te)
    XX = inv(Ac) @ (Ad - np.eye(Ad.shape[0]))
    Cd = Cc @ XX / Te
    Gammad0 = XX @ Gammac

    # Adjust discrete filter gain
    Sigmav = ajuste_sigmav(Ad, Gammad0, Cd, varphi)
    Gammad = Gammad0 @ sqrtm(Sigmav)

    # Square root of steady-state variance matrix (for initialization)
    SigmaX = solve_discrete_lyapunov(Ad, Gammad @ Gammad.T)
    RacSigmaX = sqrtm(SigmaX)
    RacSigmaX = np.real(RacSigmaX)

    return Ad, Gammad, Cd, RacSigmaX

if __name__ == "__main__":
    # Parameters
    Fe2 = 3000
    Te2 = 1 / Fe2

    modelename = 'models/modelevib0606tip.fits'
    with pfits.open(modelename) as hdul:
        data_hdu = hdul[0]
        Avibc = hdul[1].data
        Cvibc = hdul[2].data
        Gammavibc = hdul[3].data
        varvibration = hdul[4].data
        # vib_gen = vibration_generator(hdul, Fe2, 3)



    # Assuming rediscret function is defined and available
    # Obtain the discrete model matrices
    Avib2, Gammavib2, Cvib2, RacSigmaX = rediscret(Avibc, Gammavibc, Cvibc, varvibration, Fe2)

    # Simulation settings
    tsimul = 50  # Total simulation time in seconds
    nsimul = int(tsimul / Te2) - 1  # Number of simulation steps

    # Initialize the model state
    nxvib = Avib2.shape[0]
    xvib = RacSigmaX @ np.random.randn(nxvib,1)
    phivib = []#[Cvib2 @ xvib]  # Initialize phivib with the first output

    # Simulation loop
    for _ in range(nsimul+1):
        vk = np.random.randn()  # White noise input
        xvib = Avib2 @ xvib + Gammavib2 * vk  # State update
        phivibk = Cvib2 @ xvib
        # phivibk = vib_gen.step() # test vibration generator
        phivib.append(phivibk)

    phivib = np.array(phivib).flatten()

    # Variance for model output
    varmodvib = Cvib2 @ solve_discrete_lyapunov(Avib2, Gammavib2 @ Gammavib2.T) @ Cvib2.T

    # Time vector for plotting
    tsim = np.arange(0, tsimul, Te2)

    # Plot trajectory
    plt.figure(10)
    plt.plot(tsim, phivib, 'm')
    plt.xlabel('Time (seconds)')
    plt.title('Simulated trajectory')
    plt.grid(True)

    # Frequency vector for PSD plot
    npoints = len(phivib)
    ftraj = (np.arange(npoints // 2) * Fe2) / npoints

    # Calculate PSD of the simulated trajectory
    varvib2 = np.var(phivib)
    dsptraj = (np.abs(np.fft.fft(phivib)) ** 2) / npoints
    dsptraj = dsptraj[:npoints // 2]  # Take only the positive frequency part

    # Generate PSD of the vibration model

    Hvib2 = ct.ss(Avib2, Gammavib2, Cvib2, 0, dt=Te2)
    gainHvib2, _, _ = ct.frequency_response(Hvib2, ftraj*2*np.pi)

    dspmodvib2 = gainHvib2 ** 2

    # Plot PSDs
    plt.figure(11)
    plt.subplot(211)
    plt.loglog(ftraj, dsptraj, 'm', label='Simulated vibration', linewidth=1.5)
    plt.loglog(ftraj, dspmodvib2, 'k', label='Vibration model', linewidth=1.5)
    plt.legend()
    plt.ylabel('PSD (mas^2)')
    plt.title('PSD and cumulative PSD of model and simulated vibration trajectory')
    plt.grid(True)

    # Plot cumulative PSDs
    dsptot = np.sum(dsptraj)
    dspmod2tot = np.sum(dspmodvib2)
    cumulative_dsptraj = np.cumsum(dsptraj) * varvib2 / dsptot
    cumulative_dspmodvib2 = (np.cumsum(dspmodvib2) * varmodvib / dspmod2tot).squeeze()

    plt.subplot(212)
    plt.semilogx(ftraj, cumulative_dsptraj, 'm', label='Simulated vibration', linewidth=1.5)
    plt.semilogx(ftraj, cumulative_dspmodvib2, 'k', label='Vibration model', linewidth=1.5)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Cumulative PSD')
    plt.legend()
    plt.grid(True)
    plt.tight_layout()
    plt.show()
