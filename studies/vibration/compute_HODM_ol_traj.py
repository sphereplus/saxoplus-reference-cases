import numpy as np
import astropy.io.fits as pfits
from astropy.time import Time
from matplotlib import pyplot as plt
import gzip
import shutil
import os
import glob
from vibration_generator import VibrationGenerator
############################################# FUNCTION DEFINITION ############################################
def compute_psd_welch(data, fft_size, fs):
    if data.ndim == 1:
        data = data[:, np.newaxis]

    n_modes = data.shape[1]

    window_size = fft_size

    n_frames = (data.shape[0] - window_size) // (fft_size // 2) + 1
    spectrogram = np.zeros((fft_size // 2 + 1, n_frames, n_modes))

    window = np.hamming(window_size)

    for mode in range(n_modes):
        for i in range(n_frames):

            start_idx = i * (fft_size // 2)  # Overlap by 50%
            data_w = data[start_idx:start_idx + window_size, mode]
            data_w = data_w * window
            fft_result = np.fft.rfft(data_w)
            psd_w = (np.abs(fft_result)) ** 2/fs/fft_size/(np.mean(window ** 2))
            psd_w[1:-1] *= 2  # Double non-DC, non-Nyquist components
            spectrogram[:, i, mode] = psd_w

    avg_psd = np.mean(spectrogram, axis=1).squeeze()
    f = np.linspace(0, fs / 2, fft_size // 2 + 1)

    return avg_psd, f, spectrogram

def ensure_file_exists(file_path, gz_file_path):
    # Check if the file exists
    if not os.path.exists(file_path):
        print(f"{file_path} does not exist. Attempting to unzip from {gz_file_path}.")

        # Check if the .gz file exists
        if os.path.exists(gz_file_path):
            try:
                # Unzip the .gz file
                with gzip.open(gz_file_path, 'rb') as f_in:
                    with open(file_path, 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
                print(f"File extracted successfully from {gz_file_path}.")
            except Exception as e:
                print(f"An error occurred while extracting the .gz file: {e}")
        else:
            print(f".gz file {gz_file_path} does not exist.")
    else:
        print(f"File {file_path} already exists.")

def pol_reconstruct(command, measurement, delay):
    delay_floor = int(np.floor(delay))
    delay_ceil = int(np.ceil(delay))
    delay_frac,_ = np.modf(delay)
    pol = measurement[delay_ceil:,:] + (1 - delay_frac) * command[1:-delay_floor,:] + delay_frac * command[:-delay_ceil,:]
    return pol
####################################### OBSERVATION PATH, SET BY USER ########################################
model_number = 1

match model_number:
    case 0:
        date = '2024-06-06'
        model_path = "modelevib0606KL_2.fits"
        folder_name = "2024-06-07T00_07_38-VisLoopRecorder"
    case 1:
        date = '2024-06-06'
        model_path = "modelevib0606KL.fits"
        folder_name = "2024-06-07T04_13_09-VisLoopRecorder"
    case 2:
        date = '2024-06-11'
        model_path = "modelevib0611KL.fits"
        folder_name = "2024-06-12T07_54_21-VisLoopRecorder"


####################################### DATA LOADING ET COMPUTATION ###########################################
path = date+'/'+folder_name+'/'

tt_intermatrix = pfits.getdata(path+'CLMatrixOptimiser.PROJ_PAR.fits')

print('Opening the VisLoopRecorder file: ')
ensure_file_exists(path+folder_name+'.fits', path+folder_name+'.fits.gz')
VisLoopRecorder_HDU = pfits.open(path+folder_name+'.fits')
VisLoopRecorder_HDU.info()
V2ARCSEC = 2.6
VisLoopRecorder = VisLoopRecorder_HDU[1].data
ITTM_Positions = VisLoopRecorder['ITTM_Positions'].T*V2ARCSEC

slopes = VisLoopRecorder['Gradients'].T
ACT2WFS = pfits.getdata(path+'VisTTCtr.ACT2WFS_MATRIX.fits')

ensure_file_exists(path+'CLMatrixOptimiser.S2M.fits', path+'CLMatrixOptimiser.S2M.fits.gz')
S2M_name = glob.glob(os.path.join(path,'CLMatrixOptimiser.S2M.fits'))[0]
S2M = pfits.getdata(S2M_name)

ensure_file_exists(path+'CLMatrixOptimiser.V2M.fits', path+'CLMatrixOptimiser.V2M.fits.gz')
V2M_name = glob.glob(os.path.join(path,'CLMatrixOptimiser.V2M.fits'))[0]
V2M = pfits.getdata(V2M_name)

ensure_file_exists(path+'CLMatrixOptimiser.M2V.fits', path+'CLMatrixOptimiser.M2V.fits.gz')
M2V_name = glob.glob(os.path.join(path,'CLMatrixOptimiser.M2V.fits'))[0]
M2V = pfits.getdata(M2V_name)

M2V = np.linalg.pinv(V2M)
IMF = pfits.getdata('SAXO_DM_IFM.fits')
HODM_Positions = VisLoopRecorder['HODM_Positions']
median_DMshape_vector = np.median(HODM_Positions,axis=0)
HODM_Positions_NoBias = VisLoopRecorder['HODM_Positions'] - median_DMshape_vector

HODM_Positions_modal = (V2M@HODM_Positions_NoBias.T).T

rad_632_to_nm_opt = 1. / 2. / np.pi * 632 * 2 # influence matrix normalization = defoc meca in rad @ 632 nm
IMF   = IMF * rad_632_to_nm_opt
IMF   = IMF.reshape(1377, 240*240).T

seconds_VisLoopRecorder = VisLoopRecorder["Seconds"]+VisLoopRecorder["USeconds"]/1.e6
time_VisLoopRecorder = Time(seconds_VisLoopRecorder,format='unix')
size_VisLoopRecorder = len(time_VisLoopRecorder)
ellapsed_time_VisLoopRecorder = time_VisLoopRecorder[-1]-time_VisLoopRecorder[0]
# print(ellapsed_time_VisLoopRecorder.sec)

delta_t = ellapsed_time_VisLoopRecorder.sec/size_VisLoopRecorder
fs = 1./delta_t
time_ellapsed_array = seconds_VisLoopRecorder-seconds_VisLoopRecorder[0]
delay_saxo = (725e-6 + 80e-6 + 35e-6) * fs + 1  # (readout + rtc + DM rise)*fs + exposure
n_mode = 5
start_modes = 2
mode_res = (S2M@slopes)[start_modes:n_mode+start_modes,:].squeeze().T
mode_ol = np.zeros(n_mode)
M2OPD = np.zeros(n_mode)
HODM_Positions_modal = HODM_Positions_modal[:,start_modes:n_mode+start_modes]

for i in range(n_mode):
    IMF_M = IMF@M2V[:,i]
    M2OPD[i] = IMF_M[IMF_M != 0].std()
    mode_res[:,i] *= M2OPD[i]
    HODM_Positions_modal[:,i] *= M2OPD[i]
mode_ol = pol_reconstruct(HODM_Positions_modal, mode_res, delay_saxo)

nfft = 1000

mode_res_psd,f,_ = compute_psd_welch(mode_res, nfft, fs)
mode_ol_psd,f,_ = compute_psd_welch(mode_ol, nfft, fs)

########################################### VIBRATION GENERATION #############################################

model_c =  pfits.open(model_path)
vib_gen = VibrationGenerator(model_c, fs)
# vib_sim = np.zeros((5,int(30*fs)))
vib_sim = np.zeros((int(30*fs),5))
for i in range(int(30*fs)):
    vib_sim[i,:] = vib_gen.step()

vib_sim_psd,f,_ = compute_psd_welch(vib_sim, nfft, fs)


############################################# PLOTS ##########################################################


plt.figure()
for i in range(n_mode):
    plt.loglog(f, np.cumsum(mode_res_psd[:,i]))
plt.title('HODM modes residual cumulative PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel("magnitude [dB]")
plt.legend((np.arange(10)))
plt.grid()


plt.figure()
for i in range(n_mode):
    plt.loglog(f, mode_ol_psd[:,i])
plt.title('HODM KL modes pseudo open-loop PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel('PSD [arcsec^2/Hz]')
plt.legend((np.arange(10)))
plt.grid()


plt.figure()

for i in range(n_mode):
    plt.loglog(f, np.cumsum(mode_ol_psd[:,i]))
plt.title('mode open-loop cumulative PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel("magnitude [dB]")
plt.legend((np.arange(10)))
plt.grid()


plt.figure()
for i in range(n_mode):
    plt.plot(time_ellapsed_array[:len(mode_ol[:,i])],mode_ol[:,i])
plt.title('KL mode open-loop')
plt.xlabel("time [s]")
plt.ylabel("OPD [nm]")
plt.legend((np.arange(10)))
plt.grid()


pfits.writeto(path+'HODM_modal_ol_traj.fits',mode_ol, overwrite = True)


plt.figure()
plt.title('KL mode shape')
plt.imshow((IMF@M2V[:,3]).reshape((240, 240)))


# Create subplots
fig, axs = plt.subplots(n_mode, 1, figsize=(10, 2 * n_mode), sharex=True)

for i in range(n_mode):
    axs[i].loglog(f, mode_ol_psd[:, i], label=f' pseudo open-loop (Measured)', color='blue')
    axs[i].loglog(f, vib_sim_psd[:, i], linestyle='--', label=f'vibration (Simulated)', color='red')
    axs[i].set_title(f'PSD for KL mode {i}')
    axs[i].set_ylabel('PSD [nm^2/Hz]')
    axs[i].legend(fontsize=9)
    axs[i].grid(which='both', linestyle='--', linewidth=0.5)

# Shared x-axis label
axs[-1].set_xlabel('Frequency [Hz]')

# Main title
fig.suptitle('HODM KL modes PSD for model {}, from telemetry data along with extracted simulated vibration'.format(model_number))

# Adjust layout to fit everything nicely
plt.tight_layout(rect=[0, 0, 1, 0.95])


plt.show()

