import numpy as np
import astropy.io.fits as pfits
from astropy.time import Time
from matplotlib import pyplot as plt
import gzip
import shutil
import os
import dd4ao
from dd_utils import *
import glob
from scipy import signal
from vibration_generator import VibrationGenerator
############################################# FUNCTION DEFINITION ############################################
def compute_psd_welch(data, fft_size, fs):
    if data.ndim == 1:
        data = data[:, np.newaxis]

    n_modes = data.shape[1]

    window_size = fft_size

    n_frames = (data.shape[0] - window_size) // (fft_size // 2) + 1
    spectrogram = np.zeros((fft_size // 2 + 1, n_frames, n_modes))

    window = np.hamming(window_size)

    for mode in range(n_modes):
        for i in range(n_frames):

            start_idx = i * (fft_size // 2)  # Overlap by 50%
            data_w = data[start_idx:start_idx + window_size, mode]
            data_w = data_w * window
            fft_result = np.fft.rfft(data_w)
            psd_w = (np.abs(fft_result)) ** 2/fs/fft_size/(np.mean(window ** 2))
            psd_w[1:-1] *= 2  # Double non-DC, non-Nyquist components
            spectrogram[:, i, mode] = psd_w

    avg_psd = np.mean(spectrogram, axis=1).squeeze()
    f = np.linspace(0, fs / 2, fft_size // 2 + 1)

    return avg_psd, f, spectrogram
    
def ensure_file_exists(file_path, gz_file_path):
    # Check if the file exists
    if not os.path.exists(file_path):
        print(f"{file_path} does not exist. Attempting to unzip from {gz_file_path}.")

        # Check if the .gz file exists
        if os.path.exists(gz_file_path):
            try:
                # Unzip the .gz file
                with gzip.open(gz_file_path, 'rb') as f_in:
                    with open(file_path, 'wb') as f_out:
                        shutil.copyfileobj(f_in, f_out)
                print(f"File extracted successfully from {gz_file_path}.")
            except Exception as e:
                print(f"An error occurred while extracting the .gz file: {e}")
        else:
            print(f".gz file {gz_file_path} does not exist.")
    else:
        print(f"File {file_path} already exists.")


def pol_reconstruct(command, measurement, delay):
    delay_floor = int(np.floor(delay))
    delay_ceil = int(np.ceil(delay))
    delay_frac,_ = np.modf(delay)
    pol = measurement[delay_ceil:] + (1 - delay_frac) * command[1:-delay_floor] + delay_frac * command[:-delay_ceil]
    return pol


####################################### OBSERVATION PATH, SET BY USER ########################################
model_number = 0

match model_number:
    case 0:
        date = '2024-06-06'
        model_path = 'modelevib0606tiptilt.fits'
        folder_name = "2024-06-07T04_13_09-VisLoopRecorder"
    case 1:
        date = '2024-06-11'
        model_path = 'modelevib0611tiptilt.fits'
        folder_name = "2024-06-12T07_54_21-VisLoopRecorder"
    case 2:
        date = '2024-06-06'
        model_path = 'modelevib0606tiptilt_2.fits'
        folder_name = "2024-06-07T00_07_38-VisLoopRecorder"


####################################### DATA LOADING ET COMPUTATION ###########################################
path = date+'/'+folder_name+'/'

tt_intermatrix = pfits.getdata(path+'CLMatrixOptimiser.PROJ_PAR.fits')

print('Opening the VisLoopRecorder file: ')
ensure_file_exists(path+folder_name+'.fits', path+folder_name+'.fits.gz')
VisLoopRecorder_HDU = pfits.open(path+folder_name+'.fits')
VisLoopRecorder_HDU.info()
V2ARCSEC = 2.6

VisLoopRecorder = VisLoopRecorder_HDU[1].data
ITTM_Positions = VisLoopRecorder['ITTM_Positions'].T*V2ARCSEC

slopes = VisLoopRecorder['Gradients'].T
ACT2WFS = pfits.getdata(path+'VisTTCtr.ACT2WFS_MATRIX.fits')

ensure_file_exists(path+'CLMatrixOptimiser.S2M.fits', path+'CLMatrixOptimiser.S2M.fits.gz')
S2M_name = glob.glob(os.path.join(path,'CLMatrixOptimiser.S2M.fits'))[0]
S2M = pfits.getdata(S2M_name)

ensure_file_exists(path+'CLMatrixOptimiser.V2M.fits', path+'CLMatrixOptimiser.V2M.fits.gz')
V2M_name = glob.glob(os.path.join(path,'CLMatrixOptimiser.V2M.fits'))[0]
V2M = pfits.getdata(V2M_name)
HODM_Positions_modal = V2M@VisLoopRecorder['HODM_Positions'].T

WFS2ARCSEC= np.linalg.pinv(ACT2WFS)*V2ARCSEC

tt_res = WFS2ARCSEC@((tt_intermatrix@slopes)[:2,:].squeeze())
tip_res = tt_res[0,:]
tilt_res = tt_res[1,:]

tip_res_var = np.var(tip_res)
tilt_res_var = np.var(tilt_res)

seconds_VisLoopRecorder = VisLoopRecorder["Seconds"]+VisLoopRecorder["USeconds"]/1.e6
time_VisLoopRecorder = Time(seconds_VisLoopRecorder,format='unix')
size_VisLoopRecorder = len(time_VisLoopRecorder)
ellapsed_time_VisLoopRecorder = time_VisLoopRecorder[-1]-time_VisLoopRecorder[0]

# print(ellapsed_time_VisLoopRecorder.sec)
delta_t = ellapsed_time_VisLoopRecorder.sec/size_VisLoopRecorder
fs = 1./delta_t

delay_saxo = (725e-6 + 80e-6 + 35e-6)*fs + 1 # (readout + rtc + DM rise)*fs + exposure
time_ellapsed_array = seconds_VisLoopRecorder-seconds_VisLoopRecorder[0]
nfft = 1000
# nfft = int(tip_res.shape[0])

tip_res_psd,f,_ = compute_psd_welch(tip_res, nfft, fs)
tilt_res_psd,f,_ = compute_psd_welch(tilt_res, nfft, fs)

tip_ol = pol_reconstruct(ITTM_Positions[0,:],tip_res,delay_saxo)
tilt_ol = pol_reconstruct(ITTM_Positions[1,:],tilt_res,delay_saxo)

tip_ol_var = np.var(tip_ol)
tilt_ol_var = np.var(tilt_ol)

tip_ol_psd,f,_ = compute_psd_welch(tip_ol, nfft, fs)
tilt_ol_psd,f,_ = compute_psd_welch(tilt_ol, nfft, fs)

########################################### VIBRATION GENERATION #############################################

model_c =  pfits.open(model_path)
vib_gen = VibrationGenerator(model_c, fs)
vib_sim = np.zeros((2,int(30*fs)))
for i in range(int(30*fs)):
    vib_sim[:,i] = vib_gen.step()
tip_sim = vib_sim[0,:]
tilt_sim = vib_sim[1,:]
tip_sim_var = np.var(tip_sim)
tilt_sim_var = np.var(tilt_sim)

tip_sim_psd,f,_ = compute_psd_welch(tip_sim, nfft, fs)
tilt_sim_psd,f,_ = compute_psd_welch(tilt_sim, nfft, fs)

############################################# PLOTS ##########################################################

plt.figure()
plt.loglog(f, tip_res_psd)
plt.loglog(f, tilt_res_psd)
plt.title('tip tilt residual PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel("amplitude (arcsec^2/Hz)")
plt.legend(("tip",'tilt'))
plt.grid()


plt.figure()
plt.loglog(f, np.cumsum(tip_res_psd)/np.sum(tip_res_psd)*tip_res_var)
plt.loglog(f, np.cumsum(tilt_res_psd)/np.sum(tip_res_psd)*tilt_res_var)
plt.title('tip tilt residual cumulative PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel("variance [arcsec^2]")
plt.legend(("tip",'tilt'))
plt.grid()

plt.figure()
plt.loglog(f, tip_ol_psd)
plt.loglog(f, tilt_ol_psd)
plt.loglog(f, tip_sim_psd,linestyle='dashed')
plt.loglog(f, tilt_sim_psd,linestyle='dashed')
plt.title('tip open-loop PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel("amplitude (arcsec^2/Hz)")
plt.legend(("tip ",'tilt','tip_sim','tilt_sim'))
plt.grid()


plt.figure()
plt.loglog(f, np.cumsum(tip_ol_psd)/np.sum(tip_res_psd)*tip_ol_var)
plt.loglog(f, np.cumsum(tilt_ol_psd)/np.sum(tip_res_psd)*tilt_ol_var)
plt.title('tip tilt open-loop cumulative PSD')
plt.xlabel("frequency [Hz]")
plt.ylabel("variance [arcsec^2]")
plt.legend(("tip",'tilt'))
plt.grid()


plt.figure()
plt.plot(time_ellapsed_array[:len(tip_ol)],tip_ol)
plt.plot(time_ellapsed_array[:len(tip_ol)],tilt_ol)
plt.title('tip tilt open-loop')
plt.xlabel("time [s]")
plt.ylabel("amplitude [arcsec]")
plt.legend(("tip",'tilt'))
plt.grid()

fig, axs = plt.subplots(2, 1, figsize=(10, 8), sharex=True)

# Plot TIP data
axs[0].loglog(f, tip_ol_psd, label='Tip pseudo-open-loop (Measured)', color='blue', linestyle='-')
axs[0].loglog(f, tip_sim_psd, label='Tip vibration (Simulated)', color='red', linestyle='--')
axs[0].set_title('Power Spectral Density - Tip')
axs[0].set_ylabel('PSD [arcsec^2/Hz]')
axs[0].legend()

# Plot TILT data
axs[1].loglog(f, tilt_ol_psd, label='Tilt pseudo-open-loop (Measured)', color='blue', linestyle='-')
axs[1].loglog(f, tilt_sim_psd, label='Tilt vibration (Simulated)', color='red', linestyle='--')
axs[1].set_title('Power Spectral Density - Tilt')
axs[1].set_xlabel('Frequency [Hz]')
axs[1].set_ylabel('PSD [arcsec^2/Hz]')
axs[1].legend()

fig.suptitle('Tip-tilt PSD for model {}, from telemetry data along with extracted simulated vibration'.format(model_number))
# Adjust layout and show the plot
plt.tight_layout()


pfits.writeto(path+'tip_tilt_ol_traj.fits', np.array([tip_ol,tilt_ol]), overwrite = True)


plt.show()
