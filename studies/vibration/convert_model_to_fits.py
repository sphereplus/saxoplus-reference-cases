from scipy.io import loadmat
import astropy.io.fits as pfits
import numpy as np

# Load the MATLAB file
mat_data = loadmat('modelevib0611tiptilt.mat')

# Prepare an empty FITS HDU list
hdu = pfits.PrimaryHDU(data=None)  # No primary data
hdulist = pfits.HDUList([hdu])

# Process each key in the MATLAB file
for key, value in mat_data.items():
    if key.startswith('__'):  # Skip MATLAB metadata
        continue
    if isinstance(value, np.ndarray) and value.dtype.name.startswith('void'):
        print(f"Skipping MATLAB struct or unsupported type: {key} (type: {value.dtype.name})")
        continue
    if isinstance(value, (np.ndarray, int, float, list)):
        # Add arrays to ImageHDU
        hdulist.append(pfits.ImageHDU(data=value, name=key))
    else:
        print(f"Skipping unsupported key: {key} (type: {type(value)})")

# Save to FITS file
hdulist.writeto('modelevib0611tiptilt.fits', overwrite=True)
