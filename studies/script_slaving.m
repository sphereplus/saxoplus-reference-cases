% Script to compute slaving matrix
%

% load actuator positions
xact = saxoplus_act_pos(1,:);
yact = saxoplus_act_pos(2,:);
inpup = saxoplus_act_pos(3,:);

% Rescaling of spatial coordinates
% active actuators
xact = (xact + 16.5)/16 + 1;
yact = (yact + 16.5)/16 + 1;
nuact = length(xact);
% actuators in pupil
xpup = xact(inpup > 0);
ypup = yact(inpup > 0);
nupup = length(xpup);

% Square reference grid
M = max(yact);
ix = 1:M;
iy = 1:M;
[X,Y] = meshgrid(ix,iy);

% Mask of controlled actuators
ixyact = xact + M*(yact-1);
XYact = zeros(M^2,1);
XYact(ixyact) = 1;
XYact = reshape(XYact,M,M);

% Mask of actuators in pupil
ixypup = xpup + M*(ypup-1);
XYpup = zeros(M^2,1);
XYpup(ixypup) = 1;
XYpup = reshape(XYpup,M,M);

figure(1)
imagesc(XYpup + XYact)
axis('square')

% Mask with number of neighbours in pupil (weighted with distances)
poids1 = [1/sqrt(2) 1 1/sqrt(2) ; 1 0 1; 1/sqrt(2) 1 1/sqrt(2)];
XYvoisins = conv2(XYpup,poids1,'same');
XYvoisins = max(XYvoisins,1e-5);
figure(2)
imagesc(XYvoisins)
axis('square')
% Mask with number of neighbours in Mask XYvoisins1 (for step 2)
XYpup2 = XYvoisins > 0.01;
figure(3)
imagesc(XYpup + XYpup2)
axis('square')
XYvoisins2 = conv2(XYpup2,poids1,'same');
XYvoisins2 = max(XYvoisins2,1e-5);
figure(4)
imagesc(XYvoisins2)
axis('square')
% More steps can be added if needed...

% Test: slaving of a command vector defined in pupil (with values stored in
% 2D grid)
utest = 10*rand(size(X));
utest = utest .* XYpup;
figure(5)
imagesc(utest)
axis('square')
% Slaving step 1
uout1 = conv2(utest,poids1,'same') ./ XYvoisins;
uslaved1 = utest .* XYpup + uout1 .* not(XYpup);
% Slaving step 2
uout2 = conv2(uslaved1,poids1,'same') ./ XYvoisins2;
uslaved2 = uslaved1 .* XYpup2 + uout2 .* not(XYpup2);
uslaved = uslaved2 .* XYact;
figure(6)
imagesc(uslaved)
axis('square')

% Filling up slaving matrix 
Mslave = zeros(nuact,nupup);
for j = 1:nupup
    indicej = zeros(nupup,1);
    indicej(j) = 1;
    ujL = zeros(M^2,1);
    ujL(ixypup) = indicej;
    uj = reshape(ujL,M,M);
    uout1 = conv2(uj,poids1,'same') ./ XYvoisins;
    uslaved1 = uj .* XYpup + uout1 .* not(XYpup);
    uout2 = conv2(uslaved1,poids1,'same') ./ XYvoisins2;
    uslaved2 = uslaved1 .* XYpup2 + uout2 .* not(XYpup2);
    uslaved2L = uslaved2(:);
    Mslave(:,j) = uslaved2L(ixyact);
end
figure(7)
imagesc(Mslave)

% Checking that it Mslave is indeed an interpolation matrix
% (values plotted should all be equal to 1
figure(8)
plot(sum(Mslave,2))

% Visualisation of interpolation weights for a single actuator
iact = 310;
indiceact = zeros(nuact,1);
indiceact(iact) = 1;
uactL = zeros(M^2,1);
uactL(ixyact) = indiceact;
uact = reshape(uactL,M,M);
indicecoeffs = Mslave(iact,:);
coeffsL = zeros(M^2,1);
coeffsL(ixypup) = indicecoeffs;
coeffs = reshape(coeffsL,M,M);

figure(10)
%imagesc(uact + coeffs)
imagesc(uact + coeffs + 0.1*XYpup)
axis('square')




% Saving to fits and mat files
% fitswrite(Mslave,'SlavingMatrix.fits')
% save SlavingMatrix Mslave