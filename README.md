# SAXOplus reference cases and scripts

Reference cases and scripts for SPHERE+ adaptive optics simulations.

[[_TOC_]]


## Installation of the software

SAXO+ simulations should now be run with COMPASS version >= 6. In case you need to run simulations with a version 
5.x.x of COMPASS, use the branch `compass_v5`.

### Install COMPASS Version 6

COMPASS software must be installed by following these
[instructions](https://compass.pages.obspm.fr/website/page/install).

WARNING: Note that in the last part "Compilation & Installation", there may still have a typo error if it is written:
```
cd $COMPASS_ROOT
./compile_vcpkg.py
```
It should be 
```
cd $COMPASS_ROOT
./compile.py
```
since the other file does not exist.

SAXO+ simulations can now be run with the main branch of COMPASS and SHESHA.

### Download SAXOplus reference cases and run a simulation

You need of course to get the current repository `SAXOplus reference cases`. 
You should avoid downloading this repository in `$SHESHA_ROOT`.

```
$ cd <wherever you want to install this repository>
$ git clone https://git-cral.univ-lyon1.fr/sphereplus/saxoplus-reference-cases.git
```

Since COMPASS version 6, the SAXO+ DMs influence FITS files come in the `data` 
directory of the present git lab repository. If it is the first time you get the repository locally,
they require to be uncompressed by doing so:
```
$ cd data/
$ unzip HODM.zip
$ unzip BostonDMs.zip
```

As most of the files in saxoplus-reference-cases are continuously evolving, 
you need to update them to the last versions from time to time. To do so, just go to your
`saxoplus-reference-cases` directory and pull the updates from the
repository:
```
$ cd <your path to / saxoplus-reference-cases>
$ git pull
```

You can launch a short simulation example and have a look at the help of `run_simu` to go
further:

```
$ cd saxoplus-reference-cases/scripts/
$ mkdir /tmp/results
$ python run_simu --output /tmp/results -p short.toml bright1_s0_4_t9
$ python run_simu --help
```

In the previous lines, the first command `run_simu` runs a simulation for
the `bright1_s0_4_t9` case, using `refcontroller` controller (i.e. the
default controller), specifying to put the results and plots files in the
`/tmp/results` directory. You can have a look at them. The argument 
`-p short.toml` uses the parameter values written in the file short.toml 
to overwrite the simulation parameters. Here short.toml contains only one 
line: `exposure_time = 0.5` instead of the default 5 seconds of the 
dictionnary bright1 simulations.

See help (last line above) for more.


### Launch the simulation according to your taste

Note that if you want to be in interactive mode for the simulation, you
can do
```
$ python -i run_simu --output /tmp/results -p short.toml bright1_s0_4_t9
```
or 
```
$ ipython -i run_simu -- --output /tmp/results -p short.toml bright1_s0_4_t9
```
The extra `--` after run_simu are required for ipython to disentangle the two
sets of arguments.

If on the contrary, you do not care about interactive mode and only want to run 
the simulations as scripts, you can make run_simu executable and simply call:
```
$ chmod a+x run_simu
$ ./run_simu --output /tmp/results -p short.toml bright1_s0_4_t9
```

## Going further running SAXO+ simulations

### Options of run_simu

Usage of run_simu is as follows (extracted from the --help):
```
$ run_simu [options] [--] <simulation_name>...
```
with `<simulation_name>`  standing for a list of simulations names.

`<simulation_name>` can be the name of a simulation or a pattern (regular
expression) that will select simulation names. The simulation names and
parameters are read from file `dictionary_of_simulation.py`. At least one
`<simulation_name>` argument must be given.

Be aware that if no simulation name is provided to run_simu, it will do all 
the simulations of the dictionnary for this controller. This can take a very 
long time.

A simulation produces a set of data files and plots saved in files, all
collected in an output directory (see --ouputs option). Each simulation stores
its result in a separate directory. The --plot option is used to display plots
as they are produced.

The list of `Options` is:\
  -h, --help&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;show this help message and exit\
  -l, --list&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;return a list of selected simulations\
  -c, --controller=\<controller>&emsp;controller name [default: refcontroller]\
  -i, --inputs=\<input_root>&emsp;&emsp;&emsp;input root directory for the hooks.\
  -o, --outputs=\<output_root>&emsp;output root directory for the results.\
  -p, --param=\<paramfile>&emsp;&emsp;&emsp;file with other parameter values\
  --plot&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;display plots at the end of the simulation\
  --verbose&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;verbose mode (more display for debug)

See the help of run_simu for more details on each option. We provide here some examples:

### Alternative controllers

In `run_simu` the controllers are specified with option
`-c, --controller`
to select another controller than `refcontroller`, the default one.
The list of the known controllers (found in `dictionary_of_simulation.py`)
can be obtained with command:
```
$ show_simulation_parameters --controllers
```

The use of an alternative controller requires to specify an input directory where 
to find the scripts of the required actions from the controller; hereafter referred 
to as `hook files`. So run_simu must be called with option 
`-i <input_root>, --inputs=<input_root>`.

If the given path contains a directory with the same name as the controller,
the hook files are searched in this directory. If nothing is specified,
`<input_root>` is set from environment variable SAXO_INPUT_ROOT. 
So two solutions exist for a given controller:

```
  INPUT_ROOT/               - or -    INPUT_ROOT/
      |- saxoPlusConfig.py                `- <controller>/
      |- saxoPlusCalibOnSource.py            |- saxoPlusConfig.py
      |- saxoPlusStage1NewSlopes.py          |- saxoPlusCalibOnSource.py
      `- saxoPlusStage2NewSlopes.py          |- saxoPlusStage1NewSlopes.py
                                             `- saxoPlusStage2NewSlopes.py
```

For sake of simplicity, it has been decided to give access to all of the parameters
of the SAXO+ simulation manager everytime a controller would require to implement a 
specific action. However, the moments when such specific actions can be done during 
the simulation are defined and fixed by some `<hooks>`. A list of such `<hooks>` is 
proposed so far (see below) and could be increased as needed in the future. For each 
<hook>, if the controller wants to do something, there must be a `saxoPlus<hook>.py` 
file in the input directory with the required actions implemented.

You can thus define the controller of your choice by defining the corresponding python
files in your `<controller>` directory.

So far, the following hooks are taken into account:
  - `saxoPlusConfig.py` -- called when the COMPASS config is ready
  - `saxoPlusCalibOnSource.py` -- called when the COMPASS simu is ready 
  for calibration (atmosphere temporarily removed and no command applied at each loop,
  only perturbation voltages are applied)
  - `saxoPlusCalibOnSky.py` -- same as saxoPlusCalibOnSource.py, but without removing 
  the atmosphere. Calibration is made here in open loop through the atmosphere (TO BE DONE)
  - `saxoPlusStage1NewSlopes.py`  -- called when new centroids are available for the first stage
  - `saxoPlusStage2NewSlopes.py`  -- called when new centroids are available for the second stage

At initialization of the saxoPlus Manager, it will check which of the hook files exist in the 
`<controller>` directory and it will attach to the manager a list of the found hooks. During 
the simulation, at the defined hook positions, the hook file is executed in the simulator through
the python command
```
exec(open(<controllerpath>/saxoPlus<hook>.py).read())
```
Despite its possible inconveninets, this mechanism offers a wide range of controller actions to be done.

Note that depending on the position of the hook in the simulation procedure, the available variables and names vary as follows:
- when `Config` and `CalibOnSource` hook are used, the variables `<saxoplusmanager>`, `<saxo>` and `<saxoplus>` are directly available.
- when `Stage1NewSlopes` hook is used, the saxo supervisor is available as `<self>` and the manager is obtained as `<self.manager>`.
- when `Stage2NewSlopes` hook is used, the saxoplus supervisor is available as `<self>` and the manager is obtained as `<self.manager>`.

This is because the two last hooks are called inside the `next()` method of their corresponding supervisor.

#### Control of SAXO stage

A controller of SAXO first stage as close as we could (so far) to the existing controller has been
implemented in the input directory `refcontrollersaxo`. It can be used by any alternative controller 
for the second stage which does not want to implement a first stage specific controller.
For this, you only need to refer to it in your hooks `CalibOnSource` and `Stage1NewSlopes`.

1) Add to you calibration hook file `saxoPlusCalibOnSource.py`, the following line:
```
# Calibrate SAXO 1st stage if required                                                                                    
exec(open(os.path.join('your_path_to_reference_cases', '/refcontrollersaxo/saxoPlusCalibOnSource.py')).read())
```
2) Copy the `saxoPlusStage1NewSlopes.py` of refcontrollersaxo directory into you own controller directory.

You are ready to have a SAXO-like first-stage control in your simulations.

#### Simulations of STANDALONE PLUS architecture

In standalone plus architecture, the second stage controller is aware of the first stage commands through a zero-delay switch. To realistically represent the information provided by this switch in the simulations even in cases of fractional delays, we have implemented specific functions that the second stage controllers can during their computation of the command:

1. Function `saxoplus.get_saxo_applied_command()`
provides the equivalent command applied on SAXO stage during the last second stage WFS exposure. This command is usually useful to build pseudo open-loop slopes including the first stage correction.

2. Function `saxoplus.get_switch_command()`
provides the last available command from the switch just before the end of the computation of the second stage command. This is typically an information required by disentangled cascade AO controllers.



### Quick tuning of the simulation parameters

To study the effect of a parameter on a simulation, or to make a short simulation test,
you can use the option
`-p <paramfile>, --param=<paramfile>`.

It will overwrite the parameters read from `dictionary_of_simulation.py` with the contents 
of the ``<paramfile>`. It must contains lines `<parameter> = <value>`.
This allows to change a few parameters without changing the dictionary. Example of file contents:
```
# This is a comment
exposure_time = 2.0      # Reduce exposure for this simulation.
stage2_frequency = 3000  # Change 2nd stage frequency.
```

### Beware of an uncontrolled number of threads

The time spent on initialization, especially in the "Creating turbulent layers" phase, should take less than a minute, and typically about 15s. Sometimes this time increases to several minutes or even tens of minutes, which is abnormal: it is then necessary to control the number of threads.In fact, this always happens unless all the CPUs on the computer are idle before launching a simulation. Thus because of this slowdown, we can no longer run several simulations at the same time.

Here is what may happens, depending on the installation (numpy, openBLAS, openMP):
- **If all CPUs are free (load average is zero)**\
During the “Creating turbulent layers” phase, all cores are busy: one thread runs at 100%, and all other threads run at ~75%. The duration of this phase is in the 10-15s range.
- **If a CPU is 100% occupied by another process**\
Before starting the simulation, we start another process which runs one thread at 100%, while all the others are free. After starting the simulation, during the “Creating turbulent layers” phase, all available threads are now 100% used. This phase may now last up to 15 minutes!\
One explanation for this could be that the thread in the pool sharing the processor already occupied slows down the whole thread pool. All the other threads in the pool are waiting for it with spinlocks that keep them 100% busy (doing nothing...).

Solutions:
- One solution that seems to work is to limit the number of threads launched by openBLAS which may be used by numpy during the simulation. Before launching `run_simu`:
  ```
  $ export OPENBLAS_NUM_THREADS=1
  ```
  A single thread seems to be quite enough: the duration of the “Creating turbulent layers” phase comes back the 10-15s range, regardless of computer load before launching the simulation. It seems that multithreading is not very useful here.

- Another solution is to share CPUs between simulations with taskset. For example:
  ```
  taskset -c 0-12 run_simu --output /tmp/results -p short.toml bright1_s0_4_t9
  ```
  This strategy is used at LIRA.


## Description of the simulations inputs and outputs

### Simulations inputs

The simulation specific inputs for SAXO+ are available in 
`cases/dictionary_of_simulation.py`. This is a very short file compared to its ability to configure and run more than 60 different simulations of SAXO+ with many possible controllers.

TO BE UPDATED WITH A DESCRIPTION OF THE PARAMETERS.


### Simulations outputs

The directory to copy the results of the simulations is specified through the option
`-o <output_root>, --output=<output_root>`.

If nothing is specified, `<output_root>` is set from environment variable `SAXO_OUTPUT_ROO`,
or set to the current directory if `SAXO_OUTPUT_ROOT` is not defined. The output layout looks 
like this:
```
        OUTPUT_ROOT/
        `- <controller>/
           |- simus.txt                 # summary of all the simulations
           |- <simulation_name>-<date_stamp>/
           |  |- cmdStat/               # statistics on the command
           |  |- contrastCurves/
           |  |- coroImages/            # coronagraphic images
           |  |- parameters.toml        # parameters used for the simulation
           |  |- psfImages/
           |  |- results.toml           # various data from the simulation
           |  |- simulation.log         # log file of the simulation
           |  |- strehlCurves/
           |  `- targetPhaseStats/
           |- <simulation_name>-<date_stamp>/
           |  `- ...

```

File `simus.txt` is a table of some main parameters of the simulations present in this directory. Each simulation creates the directory `<simulation_name>-<date_stamp>`. Inside there are
subdirectories with the outputs of the simulations herein. This includes outputs values saved in FITS format and some automatic plots saved in PNG format.
  * strehlCurves 
    - saxo.fits
    - saxoplus.fits
    - Strehl.png
    - rtc_stats.png
  * psfImages 
    - psfAvg_saxo.fits
    - psfSigma_saxo.fits
    - psfAvg_saxoplus.fits
    - psfSigma_saxoplus.fits
    - PsfDiffraction_saxo.fits
    - PsfDiffraction_saxoplus.fits
    - SAXO_Avg.png
    - SAXO_Sigma.png
    - SAXO+_Avg.png
    - SAXO+_Sigma.png
  * targetPhaseStats 
    - phaseAvg_saxo.fits
    - phaseAvg_saxoplus.fits
    - phaseSigma_saxo.fits
    - phaseSigma_saxoplus.fits
    - SAXO_Avg.png
    - SAXO+_Avg.png
    - SAXO_Sigma.png
    - SAXO+_Sigma.png
  * coroImages 
    - saxo.fits
    - saxoplus.fits
    - SAXO.png
    - SAXO+.png
  * contrastCurves 
    - saxo.fits
    - saxoplus.fits
    - SAXO.png
    - SAXO+.png

TO BE COMPLETED WITH THE NEW OUTPUTS OF RUN_SIMU.


## Other scripts

### Alternative simulation script (slowly maintained)

Two scripts currently coexist to run SAXO+ simulations:
- `run_simu`,          new script described above (to be preferred)
- `saxoPlusScript.py`, original script which may become obsolete soon

The saxoPlusScript can be run this way

```
$ cd scripts/
$ ipython -i saxoPlusScript.py ../refcontroller/ /tmp/results/
```
It is not as robust as run_simu and has a slower maintainance frequency, so we do not 
recommend to use it. If it has a feature you like that run_simu currently does not have, 
just let us know, to upgrade run_simu.

### Simulation of a single-stage SAXO (although not necessary for SAXO+ study)

A reference simulation of SAXO can be run by doing so:

```
$ cd scripts/
$ ipython -i saxo_closed_loop.py ../cases/saxo_alone.py
```

This uses a Least-Squares default controller of COMPASS with no specific optimization. You can modify saxo_closed_loop.py to plug an external controller on it too.

Although this script only simulates the SAXO stage, it is maintained so far on this git-lab because some simulated parameters are closer to the existing SAXO system than the first stage of SAXO+ simulation, like:
- the SAXO detector pixel size
- the SPHERE APLC coronograph

## Layout of the repository (to be updated)

- `cases` - gathers parameter files for compass.

  - [`saxo_alone_dead.py`](./cases/saxo.md)  to simulate SAXO as close as possible from the current system.
  - `saxo_alone.py` to simulate SAXO system (without considering the dead actuators)
  - `saxo.py` to simulate the SAXO+ first stage
  - `saxo+.py` to simulate the SAXO+ second stage
  - `dictionnary_of_simulations.py`, a dictionnary gathering all the parameters of the SAXO+ reference simulations.

- `scripts` - scripts for running the simulations.
  - `saxo_close_loop.py` to run actual SAXO system simulations (with saxo_alone_dead.py or saxo_alone.py)
  - `run_simu`           more recent script to run SAXO+ simulations
  - `saxoPlusScript.py`  original script to run SAXO+ simulations
  - `twoStagesManager.py` to manage both stages together
  - `stage1Supervisor.py` to supervise the first stage, including the hooks
  - `stage2Supervisor.py` to supervise the second stage, including the hooks
  - `saxoPlusUtils.py`    tools used for outputs and specific SAXO+ features
  - `saxoPlusOutputs.py`  SAXO+ Outputs class for results storage, plots and savings
  - `atmosphere.py`       contains the parameters of the 35-layer atmosphere
  - `dictionnary_handler.py` functions to handle the dictionnary of simulations
  - `show_simulations_parameters` script to list the controllers, or the cases recorded in the dictionary, and show there parameters.


- `refcontrollersaxo` - specific hooks for the reference controller
  to run SAXO+ simulations  with only the first stage being controlled.
  - `saxoPlusCalibOnSource.py`

- `refcontroller` - specific hooks for the reference controller to run SAXO+
  simulations  with both stages being controlled.
  - `saxoPlusCalibOnSource.py`
  - `saxoPlusStage1NewSlopes.py`


## Documentation

- COMPASS user manual : https://compass.pages.obspm.fr/website/page/manual/

- coronagraph module of COMPASS: https://compass.pages.obspm.fr/website/tutorials/2023-07-19-coronagraph/


## Lists

- https://osulistes.univ-lyon1.fr/sympa/info/sphereplus-simu

- https://osulistes.univ-lyon1.fr/sympa/info/sphereplus-control


## Merge requests

Help for merge requests is
[here](https://docs.gitlab.com/ee/user/project/merge_requests).
