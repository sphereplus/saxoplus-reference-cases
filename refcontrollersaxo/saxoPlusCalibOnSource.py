"""
Script for computing interaction matrix on calibration source
without atmosphere for saxo+ simulation.
Command matrices are also computed.

Author: Charles Goulas - Modified by C. Bechet (2023.02.01)
"""

import os
# This file is included and executed with exec(open().read()), so __file__
# is the path of the caller (either saxoPlusScript.py or run_simu).
_my_data_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  "../refcontrollersaxo")

import numpy as np
import astropy.io.fits as pfits
from shesha.util.slopesCovariance import KLmodes
from tqdm import trange
from scipy.linalg import hadamard
import dictionary_handler as simu      # for dictionary of simulations

flag_calibrate = simu.parameter("calibrate_IM")

if flag_calibrate:

    print("FLAG_CALIBRATE = 1: matrices are recalibrated and saved in ",
          dataDir)
    # 1. Measurements of reference slopes
    saxo.wfs.set_noise(0, -1) #removing noise on SH WFS (-1 = no noise)

    #Measuring ref slopes saxo                                                               
    saxo.rtc.reset_ref_slopes(0) # reset refslopes from saxo to 0 (check)
    saxoplusmanager.iterations=0 # reset iterations count
    saxo.reset_wfs_exposure()

    # ---- Apply waffle mode
    waffle_amp = simu.parameter("waffle_amp")
    if waffle_amp:
        if simu.parameter("waffle_orientation") == '+':
            waf_path = os.path.join(_my_data_dir, "waffle",
                                          "srmcalHODM_waffle_+.fits")
        else :
            waf_path = os.path.join(_my_data_dir, "waffle",
                                          "srmcalHODM_waffle_x.fits")
        waffle_volts = pfits.getdata(waf_path)*waffle_amp
        waffle_volts = np.concatenate((waffle_volts,np.zeros(2)))
        saxo.rtc.set_perturbation_voltage(0,'waffle',waffle_volts)

    saxoplusmanager.next(do_control=False) # do a complete loop with no control computation
    saxo.rtc.do_ref_slopes(0) # meaure and load reflopes from null phase

    """
    Interaction matrix and command matrix
    """
    # parameters
    tipTiltNormalization = 3391.33  # adjusted to produce as much phase as higher order modes (ratio between TT modes and KL modes phase norm)
    
    # integrator parameters
    nbModes = int(simu.parameter("stage1_nControlled"))  # number of modes to be handled

    # modal basis computation
    modalBasis = pfits.getdata(os.path.join(_my_data_directory, "M2V_saxo.fits"))
    modalBasis = np.pad(modalBasis,((0,2),(0,0))) # add TT mirror commands
    modalBasis [:,:2] = 0 # remove first two KL modes
    modalBasis [-2:,:2] = np.eye(2) # insert TT modes
    tt_command = np.copy(modalBasis[:,:2]) # used for calibration only
    modalBasis [-2:,:2] /= tipTiltNormalization # normalize tt_modes

    nbSlopes = len(saxo.rtc.get_slopes(0))
    
    slave_index = np.array([1,2,12,13,14,15,31,32,33,53,54,78,79,105,166,198,199,233,269,305,306,344,345,
        383,423,463,464,504,607,647,648,649,687,688,689,690,691,729,730,731,771,874,
        914,915,955,995,1033,1034,1072,1073,1109,1145,1179,1180,1212,1273,1299,1300,
        1324,1325,1345,1346,1347,1363,1364,1365,1366,1376,1377])-1 # slave actuators index

    modalBasis_visible = np.delete(modalBasis,slave_index,0) # modal basis with only visible actuators

    n_acts_HODM = saxo.config.p_dms[0].get_ntotact()-slave_index.shape[0] # get number of visible actuators

    act_vis_to_all_act = np.eye(n_acts_HODM+slave_index.shape[0]) # projection matrix from visible actuators space to all actuators space, without slaving. 
    act_vis_to_all_act = np.delete(act_vis_to_all_act,slave_index,1)
    
    modalIMat = np.zeros((nbSlopes, modalBasis.shape[1]))
    
    hadamard_size = 2**int(n_acts_HODM).bit_length() # find nearest power of two
    hadamard_matrix = hadamard(hadamard_size)

    amplitudeForImat = saxo.config.p_dms[0].get_push4imat()/2
    amplitudeForImat_tt = saxo.config.p_dms[1].get_push4imat()
    C = np.zeros((nbSlopes, hadamard_size))

    for i in trange(hadamard_size): # imat for HODM using hadamard
        
        volts = np.concatenate((act_vis_to_all_act@hadamard_matrix[:n_acts_HODM,i]*amplitudeForImat,np.zeros(2)))
        saxoplusmanager.iterations=0
        saxo.reset_wfs_exposure()
        saxo.rtc.set_perturbation_voltage(0, "tmp",volts)
        saxo.next(do_control=False);
        slopes = saxo.rtc.get_slopes(0)/amplitudeForImat
        C[:,i] = slopes

    for i in trange(2):  # imat for TT DM
        volts = tt_command[:,i]*amplitudeForImat_tt
        saxoplusmanager.iterations=0
        saxo.reset_wfs_exposure()
        saxo.rtc.set_perturbation_voltage(0, "tmp",volts)
        saxoplusmanager.next(do_control=False);
        slopes = saxo.rtc.get_slopes(0)
        modalIMat[:,i] = slopes/amplitudeForImat_tt

    modalIMat[:,:2] /= tipTiltNormalization
    D = C @ np.linalg.inv(hadamard_matrix)
    D = D[:,:n_acts_HODM]
    modalIMat[:,2:] = D@modalBasis_visible[:-2,2:]

    saxo.rtc.reset_perturbation_voltage(controller_index = 0)
    #  sets the noise back to the config value
    saxo.wfs.set_noise(0, saxo.config.p_wfss[0].get_noise()) 

    # computing command matrix
    modalIMatFiltered = modalIMat[:, :nbModes].copy()
    modalCommandMatrix = np.linalg.pinv(modalIMatFiltered)
    modalBasisFiltered = modalBasis[:, :nbModes].copy()
    commandMatrix = modalBasisFiltered.dot(modalCommandMatrix)

    # saving interaction matrix and command matrix of SAXO
    pfits.writeto(os.path.join(dataDir, "C.fits"), C, overwrite = True)
    pfits.writeto(os.path.join(dataDir, "saxo_imat.fits"),
                  np.append(modalIMat, np.reshape(saxo.rtc.get_ref_slopes(0), (nbSlopes,1)), axis=1),
                  overwrite = True)
    pfits.writeto(os.path.join(dataDir, "saxo_cmat.fits"), commandMatrix,
                               overwrite = True)
    print("MATRICES COMPUTED AND SAVED IN RESULTS")

else:
    print("FLAG_CALIBRATE = 0: command matrices are restored from ",
          dataDir)
    # Get and set the ref slopes stored as the last column of the IMAT
    modalIMatAndRef = pfits.getdata(os.path.join(dataDir, "saxo_imat.fits"))
    saxo.rtc.set_ref_slopes(modalIMatAndRef[:,-1], centro_index=0)
    commandMatrix = pfits.getdata(os.path.join(dataDir, "saxo_cmat.fits"))

# Setup the gain as provided in saxo.py parameters
# Setup the control matrix    
saxo.rtc.set_command_matrix(0, commandMatrix)
saxo.rtc.set_gain(0, saxo.config.p_controllers[0].get_gain())
print("Cmat for SAXO stage is ready")



