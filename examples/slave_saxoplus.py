"""
used to determine slave actuators

Usage:
  closed_loop_tilt.py <parameters_filename> [options]

with 'parameters_filename' the path to the parameters file

Options:
  -h --help          Show this help message and exit
  -i, --interactive  keep the script interactive
  -d, --devices devices      Specify the devices
"""

from shesha.config import ParamConfig
from docopt import docopt
import numpy as np
from scipy.io import savemat, loadmat
import astropy.io.fits as pfits
from matplotlib import pyplot as plt
import os
from datetime import datetime


if __name__ == "__main__":
    arguments = docopt(__doc__)

    param_file = arguments["<parameters_filename>"]

    config = ParamConfig(param_file)

    from shesha.supervisor.compassSupervisor import CompassSupervisor as Supervisor

    if arguments["--devices"]:
        config.p_loop.set_devices([
                int(device) for device in arguments["--devices"].split(",")
        ])
    supervisor = Supervisor(config)
    
    supervisor.rtc.open_loop(0) # disable implemented controller
    supervisor.atmos.enable_atmos(False) 
    pupil = supervisor.config.p_geom.get_spupil()

    p_geom = supervisor.config.p_geom
    p_dm =  supervisor.config.p_dms[0]
    p_dm_thresh =  supervisor.config.p_dms[1]

    plt.figure()
    plt.imshow(pupil)
    plt.scatter(p_dm._xpos-p_geom.get_p1(), p_dm._ypos-p_geom.get_p1(), marker='.', color="green")
    plt.scatter(p_dm_thresh._xpos-p_geom.get_p1(), p_dm_thresh._ypos-p_geom.get_p1(), marker='.', color="red")

    pos = np.stack((p_dm._xpos-p_geom.get_p1(),p_dm._ypos-p_geom.get_p1()))
    pos_thresh = np.stack((p_dm_thresh._xpos-p_geom.get_p1(),p_dm_thresh._ypos-p_geom.get_p1()))
    mask = np.isin(pos, pos_thresh)
    mask = mask[0,:]&mask[1,:] # does not work for central obstruction




    pos_comp = pos.astype(complex)
    pos_thresh_comp = pos_thresh.astype(complex)
    pos_1d = pos_comp[0,:]**(pos_comp[1,:]/100)
    pos_thresh_1d = pos_thresh_comp[0,:]**(pos_thresh_comp[1,:]/100)
    mask = np.isin(pos_1d,pos_thresh_1d)

    plt.figure()
    plt.imshow(pupil)
    plt.scatter(pos[0,:], pos[1,:], marker='.', color="cyan")
    plt.scatter(pos[0,mask], pos[1,mask], marker='.', color="red")
    plt.title("map of saxo+ slave actuators, <0.5 illumanation")

    pos = np.row_stack((pos,mask))

    # plt.figure()
    # plt.imshow(pupil)
    # plt.scatter(pos[0,:], pos[1,:], marker='.', color="cyan")
    # plt.scatter(pos[0,pos[2,:]], pos[1,pos[2,:]], marker='.', color="red")

    pfits.writeto("saxoplus_act_pos.fits",pos)

    # plt.figure()
    # plt.imshow(pupil)
    
    slave_matrix = pfits.getdata("SlavingMatrix.fits")
    B, _ = supervisor.basis.compute_modes_to_volts_basis("KL2V")
    M2V_saxoplus = slave_matrix@B  
    pfits.writeto("M2V_saxoplus.fits",M2V_saxoplus)

    B1 = pfits.getdata("../refcontroller/M2V_saxoplus.fits")


    supervisor.rtc.set_command(0,B1[:,540])
    supervisor.next()
    supervisor.next()
    supervisor.next()
    phase = supervisor.target.get_tar_phase(0)
    plt.figure()
    plt.imshow(phase)

    supervisor.rtc.set_command(0,B[:,544])
    supervisor.next()
    supervisor.next()
    supervisor.next()
    phase = supervisor.target.get_tar_phase(0)
    plt.figure()
    plt.imshow(phase)

