"""
Script for computing interaction matrix on calibration source
without atmosphere for saxo+ simulation.
Command matrices are also computed.

Author: F.V - Modified by C. Bechet (2023.01.03)

RENAME FILE saxoPlusCalibOnSource.py and place it on the controller folder to use it.
"""

import numpy as np
import astropy.io.fits as pfits
import os
from shesha.util.slopesCovariance import KLmodes
from tqdm import trange
from scipy.linalg import hadamard

# saxo.atmos.enable_atmos(False) # disabling turbulence is already done by the manager.

flag_calibrate = 1

if flag_calibrate:

    print("FLAG_CALIBRATE = 1: matrices are recalibrated and saved in ",
          dataDir)

    # 1. Measurements of reference slopes
    
    saxo.wfs.set_noise(0, -1) #removing noise on SH WFS (-1 = no noise)
    saxoplus.wfs.set_noise(0, -1) #removing noise on PYR WFS (-1 = no noise)

    saxo.config.p_centroiders[0].set_type("cog") # use standard cog for calibration
    
    #Measuring ref slopes saxo                                                               
    saxo.rtc.reset_ref_slopes(0) # reset refslopes from saxo to 0 (check)
    
    saxoplusmanager.iterations=0 # reset iterations count
    #saxo.reset_wfs_exposure()
    saxoplusmanager.next(do_control=False) # do a complete loop with no control computation
    saxo.rtc.do_ref_slopes(0) # meaure and load reflopes from null phase

    #Measuring ref slopes saxoplus                                                           
    saxoplus.rtc.reset_ref_slopes(0) # reset refslopes from saxo to 0 (check)
    saxoplusmanager.iterations=0 # reset iterations count
    #saxo.reset_wfs_exposure()
    saxoplusmanager.next(do_control=False) # do a complete loop with no control computation
    saxoplus.rtc.do_ref_slopes(0) # meaure and load reflopes from null phase
    
    
    # 2. Start measurements of iMats
    

    # Computing modified KL basis (Gendron modal basis)
    xpos0 = saxo.config.p_dms[0]._xpos # actus positions
    ypos0 = saxo.config.p_dms[0]._ypos
    n_acts_0 = saxo.config.p_dms[0].get_ntotact() # get number of visible actuators

    xpos1 = saxoplus.config.p_dms[0]._xpos
    ypos1 = saxoplus.config.p_dms[0]._ypos
    n_acts_1 = saxoplus.config.p_dms[0].get_ntotact() # get number of visible actuators

    nAllcommands0 = saxo.rtc.get_command(0).shape[0] # of total commands for cmat I.e HDOM + TT"

    L0 = 25  # [m]
    B0, l = KLmodes(xpos0, ypos0, L0, True) #basis on saxo stage
    B1, l = KLmodes(xpos1, ypos1, L0, True) #basis on saxoplus stage
    
    B0All = np.zeros((nAllcommands0,B0.shape[1]))
    B0All[0:B0.shape[0],:] = B0

    # interaction matrix on first stage
    nModes0 = B0.shape[1]
    ampli0 = 1.0e-2 #arbitraty unit
    Nslopes0 = saxo.rtc.get_slopes(0).shape[0] # nb slopes of first stage
    Nslopes1 = saxoplus.rtc.get_slopes(0).shape[0] # nb slopes of second stage
    modal_imat0 = np.zeros((Nslopes0, nModes0))
    modal_imat01 = np.zeros((Nslopes1, nModes0))

    hadamard_size_0 = 2**n_acts_0.bit_length() # find nearest power of two
    hadamard_matrix_0 = hadamard(hadamard_size_0)

    C_0 = np.zeros((Nslopes0, hadamard_size_0))
    C_01 = np.zeros((Nslopes1, hadamard_size_0))

    # Measuring imat on saxo. (both HODM and SH (modal_imat0) + HODM and PYR (modal_imat01))
    for i in trange(hadamard_size_0):
        volts = np.concatenate((hadamard_matrix_0[:n_acts_0,i]*ampli0,np.zeros(2)))

        saxoplusmanager.iterations=0
        #saxo.reset_wfs_exposure()
        saxo.rtc.set_perturbation_voltage(0, "tmp", volts)
        saxoplusmanager.next(do_control=False); # check
    
        s1 = saxo.rtc.get_slopes(0) / ampli0
        s2 = saxoplus.rtc.get_slopes(0)  / ampli0
        C_0[:, i] = s1.copy()
        C_01[:, i] = s2.copy()

    D_0 = C_0 @ np.linalg.inv(hadamard_matrix_0)
    D_0 = D_0[:,:n_acts_0]
    modal_imat0 = D_0@B0

    D_01 = C_01 @ np.linalg.inv(hadamard_matrix_0)
    D_01 = D_01[:,:n_acts_0]
    modal_imat01 = D_01@B0

    # remove ALL pertu voltages (rest the DM)
    saxo.rtc.reset_perturbation_voltage(0)
    # sets the noise back to the config value
    saxo.wfs.set_noise(0, saxo.config.p_wfss[0].get_noise()) 


    # Measuring imat on saxo+. ( Boston and PYR (modal_imat1))
    # saxoplusmanager.next(); # check
    nmodes1 = B1.shape[1] # number of modes for second stage AO
    ampli1 = 1.0e-2
    modal_imat1 = np.zeros((Nslopes1, nmodes1))

    hadamard_size_1 = 2**n_acts_1.bit_length() # find nearest power of two
    hadamard_matrix_1 = hadamard(hadamard_size_1)


    C_1 = np.zeros((Nslopes1, hadamard_size_1))

    for i in trange(hadamard_size_1):
        volts = hadamard_matrix_1[:n_acts_1,i] * ampli1; 
        saxoplus.rtc.set_perturbation_voltage(0, "tmp", volts)
        saxoplusmanager.iterations=0
        saxoplusmanager.next(do_control=False); # check
        s = saxoplus.rtc.get_slopes(0)  / ampli1
        C_1[:, i] = s.copy()

    D_1 = C_1 @ np.linalg.inv(hadamard_matrix_1)
    D_1 = D_1[:,:n_acts_1]
    modal_imat1 = D_1@B1

    #remove ALL pertu voltages (rest the DM)
    saxoplus.rtc.reset_perturbation_voltage(0)
    #  sets the noise back to the config value
    saxoplus.wfs.set_noise(0, saxoplus.config.p_wfss[0].get_noise()) 

    # Computing command matrix first stage (HODM only). TT done by HODM. 
    nControlled0 = 800
    imat0f = modal_imat0[:, :nControlled0].copy()
    # modal command matrix
    Dmp0 = np.dot(np.linalg.inv(np.dot(imat0f.T, imat0f)), imat0f.T)
    # cmat for first stage only (HODM only)
    cmat0 = B0[:, :nControlled0].dot(Dmp0) 

    #here cmat contains all DMs actuators beware
    cmatsaxo = np.zeros((nAllcommands0, Nslopes0))
    # filling HODM cmat on meta command matrix with all actus
    cmatsaxo[0:cmat0.shape[0],:] = cmat0

    # Computing command matrices second stage. 
    nControlled1 = 200
    imat1f = imat1[:, :nControlled1].copy()
    Dmp1 = np.dot(np.linalg.inv(np.dot(imat1f.T, imat1f)), imat1f.T) # modal command matrix
    cmat1 = B1[:, :nControlled1].dot(Dmp1)
    
    saxo.config.p_centroiders[0].set_type("wcog") # set back centroider to wcog

    # saving interaction matrix and command matrix of SAXO and SAXO+
    pfits.writeto(dataDir+"saxo_imat.fits", modal_imat0)
    pfits.writeto(dataDir+"saxo_cmat.fits", cmatsaxo)
    pfits.writeto(dataDir+"saxoplus_imat.fits", modal_imat1)
    pfits.writeto(dataDir+"saxoplus_cmat.fits", cmat1
    print("Matrices are saved.")

else:

    print("FLAG_CALIBRATE = 0: matrices are restored from ",
          dataDir)
    cmatsaxo = pfits.getdata(dataDir + "saxo_cmat.fits")
    cmat1 = pfits.getdata(dataDir + "saxoplus_cmat.fits")

    
# loading cmat in COMPASS (first stage)
saxo.rtc.set_command_matrix(0, cmatsaxo) 
gain = 0.3
saxo.rtc.set_gain(0, gain)
print("Cmat for SAXO stage is ready")


saxoplus.rtc.set_command_matrix(0, cmat1)
gain = 0.3
saxoplus.rtc.set_gain(0, gain)
print("Cmat for SAXO+ stage is ready")

