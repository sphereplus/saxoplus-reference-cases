"""
Script for computing interaction matrix on calibration source
without atmosphere for saxo+ simulation.
Command matrices are also computed.

Author: F.V - Modified by C. Bechet (2023.01.03)
"""
import os

# Calibrate SAXO 1st stage if required
# This file is included and executed with exec(open().read()), so __file__
# is the path of the caller (either saxoPlusScript.py or run_simu).
_my_controllersaxo_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  "../refcontrollersaxo")
exec(open(os.path.join(_my_controllersaxo_directory,"saxoPlusCalibOnSource.py")).read())


_my_data_directory = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  "../refcontroller")

import numpy as np
import astropy.io.fits as pfits
from shesha.util.slopesCovariance import KLmodes
from tqdm import trange
import dictionary_handler as simu      # for dictionary of simulations

flag_calibrate = simu.parameter("calibrate_IM")

if flag_calibrate:

    print("FLAG_CALIBRATE = 1: matrices are recalibrated and saved in",
          dataDir)

    # 1. Measurements of reference slopes
    saxoplus.wfs.set_noise(0, -1) #removing noise on PYR WFS (-1 = no noise)
    saxoplus.rtc.reset_ref_slopes(0) # reset refslopes from saxo to 0 (check)
    saxoplusmanager.iterations=0 # reset iterations count

    # ---- Apply waffle mode
    waffle_amp = simu.parameter("waffle_amp")
    if waffle_amp:
        if simu.parameter("waffle_orientation") == '+':
            waf_path = os.path.join(_my_data_dir, "waffle",
                                          "srmcalHODM_waffle_+.fits")
        else :
            waf_path = os.path.join(_my_data_dir, "waffle",
                                          "srmcalHODM_waffle_x.fits")
        waffle_volts = pfits.getdata(waf_path)*waffle_amp
        waffle_volts = np.concatenate((waffle_volts,np.zeros(2)))
        saxo.rtc.set_perturbation_voltage(0,'waffle',waffle_volts)

    saxoplusmanager.next(do_control=False) # do a complete loop with no control computation
    saxoplus.rtc.do_ref_slopes(0) # measure and load reflopes from null phase

    # 2. Start measurements of iMats
    # Computing modified KL basis (Gendron modal basis)
    xpos1 = saxoplus.config.p_dms[0]._xpos
    ypos1 = saxoplus.config.p_dms[0]._ypos
    L0 = 25  # [m]

    # modal basis computation
    B1 = pfits.getdata(os.path.join(_my_data_directory, "M2V_saxoplus.fits"))
    Nslopes1 = saxoplus.rtc.get_slopes(0).shape[0] # nb slopes of second stage

    # Measuring imat on saxo+. ( Boston and PYR (imat1))
    nmodes1 = B1.shape[1] # number of modes for second stage AO
    ampli1 = 1.0e-2
    imat1 = np.zeros((Nslopes1, nmodes1))

    for mode in trange(nmodes1):
        volts = B1[:, mode] * ampli1;
        saxoplus.rtc.set_perturbation_voltage(0, "tmp", volts)
        saxoplusmanager.iterations=0
        saxoplusmanager.next(do_control=False); # check
        s = saxoplus.rtc.get_slopes(0)  / ampli1
        imat1[:, mode] = s.copy()
    #remove ALL pertu voltages (rest the DM)
    saxoplus.rtc.reset_perturbation_voltage(0)
    #  sets the noise back to the config value
    saxoplus.wfs.set_noise(0, saxoplus.config.p_wfss[0].get_noise())

    # Computing command matrices second stage.
    nControlled1 = int(simu.parameter("stage2_nControlled"))
    imat1f = imat1[:, :nControlled1].copy()
    Dmp1 = np.dot(np.linalg.inv(np.dot(imat1f.T, imat1f)), imat1f.T) # modal command matrix
    cmat1 = B1[:, :nControlled1].dot(Dmp1)

    # saving interaction matrix and command matrix of SAXO+
    # The ref slopes must be saved together with the iMat
    pfits.writeto(os.path.join(dataDir, "saxoplus_imat.fits"),
                  np.append(imat1, np.reshape(saxoplus.rtc.get_ref_slopes(0), (Nslopes1, 1)), axis=1),
                  overwrite = True)
    pfits.writeto(os.path.join(dataDir, "saxoplus_cmat.fits"), cmat1,
                  overwrite = True)
    print("Matrices are saved.")

else:

    print("FLAG_CALIBRATE = 0: matrices are restored from ",
          dataDir)
    # Get and set the ref slopes stored as the last column of the IMAT
    imat1 = pfits.getdata(os.path.join(dataDir, "saxoplus_imat.fits"))
    saxoplus.rtc.set_ref_slopes(imat1[:,-1], centro_index=0)
    cmat1 = pfits.getdata(os.path.join(dataDir, "saxoplus_cmat.fits"))


# loading cmat in COMPASS (first stage)
saxoplus.rtc.set_command_matrix(0, cmat1)
saxoplus.rtc.set_gain(0, saxoplus.config.p_controllers[0].get_gain())
print("Cmat for SAXO+ stage is ready")

